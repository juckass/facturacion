<?php

return [
	'default' => 'pagina',

	'path' => base_path('resources/themes'),

	'cache' => [
		'enabled' => true,
		'key' => 'camaleon24.pagina',
		'lifetime' => 86400,
	]
];