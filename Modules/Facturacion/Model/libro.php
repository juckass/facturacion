<?php  
namespace Modules\Facturacion\Model;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
class libro extends model
{
	protected $table = 'libro';

	 /**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'fecha',
		'desde',
		'hasta',
		'total',
		'sucursal_id',
		'controlfacturacion_id',
	]; 

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = ['created_at', 'updated_at'];

	 /*public function setFechaAttribute($value)
	{
		// 2016-06-27
		$formato = 'd-m-Y';
		if (preg_match('/^\d{4}-\d{1,2}-\d{1,2}$/', $value)){
			$formato = 'Y-m-d';
		}
		
		$this->attributes['fecha'] = Carbon::createFromFormat($formato, $value);
	}
*/
	public function getFechaAttribute($value){
		return Carbon::parse($value)->format('d/m/Y');
	}

	public function clientes()
    {
        return $this->belongsTo('Modules\Facturacion\Model\clientes');
    }

    public function controlfacturacion()
    {
        return $this->belongsTo('Modules\Facturacion\Model\controlfacturacion');
    }
}