<?php  
namespace Modules\Facturacion\Model;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class pendientes extends model
{
	protected $table = 'pendientes';

	 /**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [	
		'ci',
		'fecha',
		'monto',
		'banco',
		'estatus',
	]; 

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = ['created_at', 'updated_at'];
	
	public function setFechaAttribute($value)
	{
		// 2016-06-27
		$formato = 'd/m/Y';
		if (preg_match('/^\d{4}-\d{1,2}-\d{1,2}$/', $value)){
			$formato = 'Y-m-d';
		}
		
		$this->attributes['fecha'] = Carbon::createFromFormat($formato, $value);
	}
	
	public function getFechaAttribute($value){
		return Carbon::parse($value)->format('d/m/Y');
	}
}