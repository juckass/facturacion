<?php
namespace Modules\Facturacion\Model;

use Illuminate\Database\Eloquent\Model;

class afacturar extends Model
{
	protected $table = 'afacturar';
    protected $fillable = ['id', 'ci', 'fecha', 'total', 'correlativo', 'sucursal_id', 'controlfacturacion_id'];

    //protected $hidden = ['created_at', 'updated_at'];
}