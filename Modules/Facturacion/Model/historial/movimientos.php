<?php  
namespace Modules\Facturacion\Model\historial;
use Modules\Admin\Model\modelo;

class movimientos extends modelo
{
	protected $table = 'movimientos_historial';

	 /**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [	
		'ci',
		'fecha',
		'monto',
		'banco',
		'estatus',
		'controlfacturacion_id'
	]; 

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

	public function controlfacturacion()
    {
        return $this->belongsTo('Modules\Facturacion\Model\controlfacturacion');
    }		

    public function cliente()
    {
        return $this->belongsTo('Modules\Facturacion\Model\clientes');
    }		
	
	
}