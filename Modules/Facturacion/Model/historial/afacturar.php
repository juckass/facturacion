<?php  
namespace Modules\Facturacion\Model\historial;
use Modules\Admin\Model\modelo;

class afacturar extends modelo
{
	protected $table = 'afacturar_historial';

	 /**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		
		'ci',
		'fecha',
		'total',
		'banco',
		'estatus',
		'totalmovi',
		'sucursal_id',
		'controlfacturacion_id',
	]; 

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

	public function controlfacturacion()
    {
        return $this->belongsTo('Modules\Facturacion\Model\controlfacturacion');
    }		

    public function sucursales()
    {
        return $this->belongsTo('Modules\Facturacion\Model\definiciones\sucursal');
    }
    
    public function cliente()
    {
        return $this->belongsTo('Modules\Facturacion\Model\clientes');
    }		
	
	
}