<?php  
namespace Modules\Facturacion\Model\historial;
use Modules\Admin\Model\modelo;

class libro extends modelo
{
	protected $table = 'libro_historial';

	 /**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'fecha',
		'desde',
		'hasta',
		'total',
		'sucursal_id',
		'controlfacturacion_id',
	]; 

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

	public function clientes()
    {
        return $this->belongsTo('Modules\Facturacion\Model\definiciones\clientes');
    }

    public function controlfacturacion()
    {
        return $this->belongsTo('Modules\Facturacion\Model\controlfacturacion');
    }
	
}