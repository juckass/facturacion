<?php  
namespace Modules\Facturacion\Model;
use Modules\Admin\Model\modelo;

class clientes extends modelo
{
	protected $table = 'clientes';

	 /**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'nacionalidad',
		'ci',
		'nombre',
		'direccion',
		'estado',
		'ciudad',
		'sucursal_id'
	]; 

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

 	public function sucursales()
    {
        return $this->belongsTo('Modules\Facturacion\Model\definiciones\sucursal');
    }

     public function movimientos_historial()
    {
        return $this->hasMany('Modules\Facturacion\Model\historial\movimientos');
    }	
    public function afacturar_historial()
    {
        return $this->hasMany('Modules\Facturacion\Model\historial\afacturar');
    }	
}