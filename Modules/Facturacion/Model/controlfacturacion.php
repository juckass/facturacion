<?php  
namespace Modules\Facturacion\Model;
use Modules\Admin\Model\modelo;

use Carbon\Carbon;

class controlfacturacion extends modelo
{
	protected $table = 'controlfacturacion';

	 /**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		
		'fecha_inicio',
		'fecha_final',
		'total_registros',
		'total_bolivares',
		'fecha_update_client',
		'total_vinculados',
		'total_pendientes',
		'total_facturado',
		'mes',
		'ano',
		'estatus'
	]; 

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = ['created_at', 'updated_at', 'deleted_at'];


	public function movimientos()
    {
        return $this->hasMany('Modules\Facturacion\Model\movimientos');
    }

    public function afacturar()
    {
        return $this->hasMany('Modules\Facturacion\Model\afacturar');
    }
	
}