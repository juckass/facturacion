<?php  
namespace Modules\Facturacion\Model\definiciones;
use Modules\Admin\Model\modelo;

class sucursal extends modelo
{
	protected $table = 'sucursal';

	 /**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'nombre',
		'abreviatura',
		'cod_sucursal',
		'correlativo'
	]; 

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

	public function sucursales()
    {
        return $this->belongsTo('Modules\Facturacion\Model\definiciones\sucursal');
    }

    public function afacturar()
    {
        return $this->hasMany('Modules\Facturacion\Model\afacturar');
    }
	
}