<?php namespace Modules\Facturacion\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Facturacion\Model\definiciones\sucursal;

class sucursales extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      
			sucursal::create([
				'nombre'		=>'Ciudad bolivar',
				'abreviatura'	=>'01-CBO',
				'cod_sucursal'	=>1,
				'corelativo'	=>0	
			]);

			sucursal::create([
				'nombre'		=>'Puerto Ordaz',
				'abreviatura'	=>'02-POZ',
				'cod_sucursal'	=>2,
				'corelativo'	=>0	
			]);
			
			sucursal::create([
				'nombre'		=>'El tigre',
				'abreviatura'	=>'03-TGR',
				'cod_sucursal'	=>3,
				'corelativo'	=>0	
			]);

			sucursal::create([
				'nombre'		=>'Puerto la cruz',
				'abreviatura'	=>'04-PLC',
				'cod_sucursal'	=>4,
				'corelativo'	=>0	
			]);
    }
}
