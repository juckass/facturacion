<?php namespace Modules\Facturacion\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Admin\Model\app_menu as menu;

class menuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menu = menu::create([
			'nombre' => 'Definiciones',
			'padre' => 0,
			'posicion' => 1,
			'direccion' => '#Definiciones',
			'icono' => 'fa fa-hashtag'
		]);

			menu::create([
				'nombre' => 'Sucursal',
				'padre' => $menu->id,
				'posicion' => 0,
				'direccion' => 'facturacion/definiciones/sucursal',
				'icono' => 'fa fa-industry'
			]);

			menu::create([
				'nombre' => 'Clientes',
				'padre' => $menu->id,
				'posicion' => 1,
				'direccion' => 'facturacion/definiciones/clientes',
				'icono' => 'fa fa-users'  
			]);

		$carga = menu::create([
			'nombre' => 'Carga',
			'padre' => 0,
			'posicion' => 2,
			'direccion' => '#cargas',
			'icono' => 'fa fa-upload'
		]);	

			menu::create([
					'nombre' => 'Clientes',
					'padre' => $carga->id,
					'posicion' => 1,
					'direccion' => 'facturacion/carga',
					'icono' => 'fa fa-users'  
				]);

		$facturacion = menu::create([
			'nombre' => 'Facturacion',
			'padre' => 0,
			'posicion' => 3,
			'direccion' => '#facturacion',
			'icono' => 'fa fa-clipboard'
		]);	
			 menu::create([
				'nombre' => 'Facturacion',
				'padre' =>  $facturacion ->id,
				'posicion' => 1,
				'direccion' => 'facturacion/facturacion',
				'icono' => 'fa fa-plus-circle'
			]); 

			 menu::create([
				'nombre' => 'Vinculacion',
				'padre' =>  $facturacion ->id,
				'posicion' => 2,
				'direccion' => 'facturacion/vincular',
				'icono' => 'fa fa-cogs'
			]);	

			menu::create([
				'nombre' => 'Afacturar',
				'padre' =>  $facturacion ->id,
				'posicion' => 3,
				'direccion' => 'facturacion/afacturar',
				'icono' => 'fa fa-cogs'
			]);	

			menu::create([
				'nombre' => 'libro',
				'padre' =>  $facturacion ->id,
				'posicion' => 4,
				'direccion' => 'facturacion/libro',
				'icono' => 'fa fa-cogs'
			]);	
			menu::create([
				'nombre' => 'cierre',
				'padre' =>  $facturacion ->id,
				'posicion' => 5,
				'direccion' => 'facturacion/cierre',
				'icono' => 'fa fa-cogs'
			]);	
    }
}
