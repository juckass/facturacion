<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TablasFacturacion extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up(){

		Schema::create('controlfacturacion', function (Blueprint $table) {
			$table->increments('id');
			$table->date('fecha_inicio');
			$table->date('fecha_final')->nullable();
			$table->date('fecha_update_client');
			$table->integer('total_registros')->unsigned()->nullable();
			$table->decimal('total_bolivares', 10, 2)->unsigned()->nullable();
			$table->integer('total_vinculados')->unsigned()->nullable();
			$table->integer('total_pendientes')->unsigned()->nullable();
			$table->decimal('total_facturado', 10, 2)->unsigned()->nullable();
			$table->integer('mes')->unsigned();
			$table->integer('ano')->unsigned();
			$table->integer('estatus')->unsigned();

			$table->unique(['mes','ano'],'controlfacturacion_index_unique');
			$table->timestamps();
			$table->softDeletes();
		});


		Schema::create('sucursal', function (Blueprint $table) {
			$table->increments('id');
			$table->string('nombre', 80)->unique();
			$table->string('abreviatura', 80)->unique();
			$table->integer('cod_sucursal')->unsigned();
			$table->integer('corelativo')->unsigned();
			$table->timestamps();
			$table->softDeletes();
		});

		Schema::create('clientes', function (Blueprint $table) {
			$table->increments('id');
			$table->string('nacionalidad', 2);
			$table->integer('ci')->unique()->unsigned();
			$table->string('nombre', 80)->nullable();
			$table->string('direccion')->nullable();
			$table->string('estado')->nullable();
			$table->string('ciudad')->nullable();
			$table->integer('sucursal_id')->unsigned();
			$table->timestamps();
			$table->softDeletes();
			$table->foreign('sucursal_id')
				->references('id')->on('sucursal')
				->onDelete('cascade')->onUpdate('cascade');
		});

		Schema::create('movimientos', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('ci')->unsigned();
			$table->date('fecha');
			$table->decimal('monto', 10, 2);
			$table->string('banco');
			$table->string('estatus', 2)->coment('
				0 = cargado
				1 = vinculado
				2 = rechazado
			');
			$table->integer('controlfacturacion_id')->unsigned();
			
			$table->foreign('controlfacturacion_id')
				->references('id')->on('controlfacturacion')
				->onDelete('cascade')->onUpdate('cascade');
			$table->timestamps();
			//$table->softDeletes();
		});

		Schema::create('pendientes', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('ci')->unsigned();
			$table->date('fecha');
			$table->decimal('monto', 10, 2);
			$table->string('banco');
			$table->string('estatus', 2);
			$table->timestamps();
			//$table->softDeletes();
		});	

		Schema::create('libro', function (Blueprint $table) {
			$table->increments('id');	
			$table->date('fecha');
			$table->integer('desde')->unsigned();
			$table->integer('hasta')->unsigned();
			$table->decimal('total', 10, 2);
			$table->integer('sucursal_id')->unsigned();
			$table->integer('controlfacturacion_id')->unsigned();


			$table->foreign('controlfacturacion_id')
				->references('id')->on('controlfacturacion')
				->onDelete('cascade')->onUpdate('cascade');

			$table->foreign('sucursal_id')
				->references('id')->on('sucursal')
				->onDelete('cascade')->onUpdate('cascade');	
			$table->timestamps();
			//$table->softDeletes();
		});	


		Schema::create('afacturar', function (Blueprint $table) {

			$table->increments('id');
			$table->integer('ci')->unsigned();
			$table->date('fecha');
			$table->decimal('total', 10, 2);
		
			$table->integer('correlativo')->unsigned()->nullable();

			$table->integer('sucursal_id')->unsigned();
			$table->integer('controlfacturacion_id')->unsigned();
			
			$table->foreign('controlfacturacion_id')
				->references('id')->on('controlfacturacion')
				->onDelete('cascade')->onUpdate('cascade');

			$table->foreign('sucursal_id')
				->references('id')->on('sucursal')
				->onDelete('cascade')->onUpdate('cascade');	

			$table->timestamps();
	
		});

	//-------------------------------------------------------------------------------------------------------------//

		//historial

		Schema::create('movimientos_historial', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('ci')->unsigned();
			$table->date('fecha');
			$table->decimal('monto', 10, 2);
			$table->string('banco');
			$table->string('estatus', 2);
			$table->integer('controlfacturacion_id')->unsigned();
		
			
			$table->foreign('controlfacturacion_id')
				->references('id')->on('controlfacturacion')
				->onDelete('cascade')->onUpdate('cascade');
			
			$table->timestamps();
			$table->softDeletes();
		});

		Schema::create('afacturar_historial', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('ci')->unsigned();
			$table->date('fecha');
			$table->decimal('total', 10, 2);
	
			$table->integer('correlativo')->unsigned()->nullable();

			$table->integer('sucursal_id')->unsigned();
			$table->integer('controlfacturacion_id')->unsigned();
			
			$table->foreign('controlfacturacion_id')
				->references('id')->on('controlfacturacion')
				->onDelete('cascade')->onUpdate('cascade');

			$table->foreign('sucursal_id')
				->references('id')->on('sucursal')
				->onDelete('cascade')->onUpdate('cascade');	

			$table->timestamps();
			$table->softDeletes();
		});

		Schema::create('libro_historial', function (Blueprint $table) {
			$table->increments('id');	
			$table->date('fecha');
			$table->integer('desde')->unsigned();
			$table->integer('hasta')->unsigned();
			$table->decimal('total', 10, 2);

			$table->integer('sucursal_id')->unsigned();
			$table->integer('controlfacturacion_id')->unsigned();


			$table->foreign('controlfacturacion_id')
				->references('id')->on('controlfacturacion')
				->onDelete('cascade')->onUpdate('cascade');
			
			$table->foreign('sucursal_id')
				->references('id')->on('sucursal')
				->onDelete('cascade')->onUpdate('cascade');	
			$table->timestamps();
			$table->softDeletes();
		});	


	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down(){
		
		
	
		Schema::dropIfExists('movimientos');
		Schema::dropIfExists('afacturar');
		Schema::dropIfExists('pedientes');
		Schema::dropIfExists('libro');
		

		/*historial*/

		Schema::dropIfExists('movimientos_historial');
		Schema::dropIfExists('afacturar_historial');
		Schema::dropIfExists('libro_historial');
		//--------------------//

		Schema::dropIfExists('controlfacturacion');
		Schema::dropIfExists('clientes');
		Schema::dropIfExists('sucursal');

	}
}
