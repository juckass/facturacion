<?php namespace Modules\Facturacion\Http\Controllers;
  
use DB;
use Illuminate\Http\Request;
use Excel; 
use Modules\Facturacion\Http\Controllers\Controller;
use Modules\Facturacion\Model\controlfacturacion;
use Modules\Facturacion\Model\clientes;



class ClientesController extends Controller {
	
	protected $titulo = 'Carga de Clientes';
	
	public $librerias = [
		
		'ladda', 
		
	];
	public $js=[
		'clientes'
	];
	
	public $css =[
		'clientes'
	];
	 
	public function getIndex()
	{
		return view('facturacion::carga', $this->_app());
	}



	public function postArchivo(Request $request){
	set_time_limit(0);
		$mimes = [
			'text/csv',
			'application/vnd.ms-excel',
			'application/vnd.oasis.opendocument.spreadsheet',
			'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
		];

		$ruta = public_path('archivos/');
		$archivo = $request->file('subir');

		$mime = $archivo->getClientMimeType();

		/*if (!in_array($mime, $mimes)) {
			return "Error de Validación, la extension el archivo cargado debe ser: xls,xlsx,csv";
		}*/
		
		do
		{
			$nombre_archivo = str_random(20) . '.' . $archivo->getClientOriginalExtension();
		} while (is_file($ruta . $nombre_archivo));
			
		$archivo->move($ruta, $nombre_archivo);
		chmod($ruta . $nombre_archivo, 0777);

		$excel = Excel::selectSheetsByIndex(0)->load($ruta . $nombre_archivo)->get();

		//dd($excel);
	
		DB::beginTransaction();

		try {
			$clie=0;
			foreach ($excel as $key => $value) {
				
				if($value->cedula != 0){ 	
					$sucursal =$value->sucursal;
					//sucursal
					if ($sucursal == 5) {
						$sucursal = 3;
					} elseif ($sucursal == 6) {
						//$sucursal = 4;
						$sucursal = 1;
						$direccion = 'CIUDAD BOLIVAR';
						$estado = 'BOLIVAR';
						$ciudad = 'CIUDAD BOLIVAR';

					} elseif (($sucursal < 1) || ($sucursal > 5)) {
						$sucursal = 1;
					}
					//fin sucursal

					// Si direccion esta vacia se le asigna segun la sucursal
							$direccion =$value->direccion;
							if (mb_strlen($direccion) < 5) {
								switch ($sucursal) {
									case 1:
										$direccion = 'CIUDAD BOLIVAR';
										break;
									case 2:
										$direccion = 'PUERTO ORDAZ';
										break;
									case 3:
										$direccion = 'EL TIGRE';
										break;
									case 4:
										$direccion = 'PUERTO LA CRUZ';
										break;
								}
							}
							//FIN ciudad vacia
							//Estado
							$estado=$value->estado;
							if ($estado == 'BOLIVAR') {
								$estado = 'BOLIVAR';
							} elseif ($estado == 'ANZOATEGUI') {
								$estado = 'ANZOATEGUI';
							} else {
								//Si el estado es desconocido asigna el de la sucursal
								if (($sucursal == 1) || ($sucursal == 2)) {
									$estado = 'BOLIVAR';
								}
								if (($sucursal == 3) || ($sucursal == 4)) {
									$estado = 'ANZOATEGUI';
								} else {
									$estado;
								}
							}
							//fin estado
							$ciudad=$value->ciudad;
							if ($ciudad == 'CIUDAD BOLIVAR') {
								$ciudad = 'CIUDAD BOLIVAR';
							} elseif (($ciudad == 'PUERTO ORDAZ') || ($ciudad == 'CIUDAD GUAYANA')) {
								$ciudad = 'PUERTO ORDAZ';
							} elseif ($ciudad == 'EL TIGRE') {
								$ciudad = 'EL TIGRE';
							} elseif ($ciudad == 'PUERTO LA CRUZ') {
								$ciudad = 'PUERTO LA CRUZ';
							} else {
								//Si la ciudad es desconocida asigna la de la sucursal

								if ($sucursal == 1) {

									$ciudad = 'Ciudad Bolivar';

								}if ($sucursal == 2) {

									$ciudad = 'Puerto Ordaz';

								}if ($sucursal == 3) {

									$ciudad = 'Tigre';

								}if ($sucursal == 4) {

									$ciudad = 'Puerto la cruz';
								}
							}
							//fin ciudad
					

					$cliente = clientes::firstOrNew([
							'nacionalidad' => $value->nacionalidad,
							'ci'		   => $value->cedula	
						]);

						$cliente->nombre		=$value->nombrecliente;
						$cliente->direccion		=$direccion;
						$cliente->estado		=$estado;
						$cliente->ciudad		=$ciudad;
						$cliente->sucursal_id	=$sucursal;

						$cliente->save();
					$clie++;
				}
			}
			$this->verificar();

		} catch (Exception $e) {
			DB::rollback();
			return $e->errorInfo[2];
		}

		DB::commit();

		return ['s' => 's', 'msj' => 'Carga Completa', 'clie' => $clie ];
	}

	public function verificar()
	{
	 	$cantidad = DB::table('controlfacturacion')
	 			->select('estatus')
	 			->where('estatus', '=', '1')
				->count();

	 	if($cantidad == 1){

		 	$rs = controlfacturacion::where('estatus', '=', 1)->first();
			$id = $rs['id'];
			controlfacturacion::find($id)->update([
			'fecha' => date('Y-m-d'),
			]);

	 	}

	 	return 0; //no activo
	}
}  
