<?php

namespace Modules\Facturacion\Http\Controllers;

//Controlador Padre
use Modules\Facturacion\Http\Controllers\Controller;

//Dependencias
use DB;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

//Request
use Modules\Facturacion\Http\Request\afacturarRequest;

//Modelos
use Modules\Facturacion\Model\afacturar;
use Modules\Facturacion\Model\movimientos;
use Modules\Facturacion\Model\definiciones\sucursal;

class AfacturarController extends Controller {
	protected $titulo = 'Afacturar';

	public $librerias = [
		'ladda'
	];

	public function __construct(){
		parent::__construct();
	}

	public function getIndex() {
		

		return $this->view('facturacion::afacturar',[
			'control' => $this->verificar(),
		]);
		
	}

	public function verificar()
	{
	 	$cantidad = DB::table('controlfacturacion')
	 			->select('estatus')
	 			->where('estatus', '=', '1')
				->count();

	 	if($cantidad > 0){
	 		return 1; //activo
	 	}
	 	return 0; //no activo
	}

	public function getBuscar(Request $request){

		$id = $request->id;

		$rs=sucursal::find($id);
		$correlativo=$rs['correlativo'];

		$movimientos = DB::table('movimientos')
		->select(DB::RAW('SUM(movimientos.monto) as monto'),DB::RAW('COUNT(movimientos.id) as movimientos'))
		->join('clientes', 'movimientos.ci', '=', 'clientes.ci')
		->join('sucursal', 'clientes.sucursal_id', '=', 'sucursal.id')
		->where('sucursal.id', '=', $id)
		->groupBy('sucursal.nombre')
		->first();

		$afacturar = DB::table('afacturar')
		->select(DB::RAW('SUM(afacturar.total) as monto'),DB::RAW('COUNT(afacturar.id) as afacturar'), DB::RAW('COUNT(afacturar.correlativo) as correlativo'))
		->join('clientes', 'afacturar.ci', '=', 'clientes.ci')
		->where('afacturar.sucursal_id', '=', $id)
		//->groupBy('sucursal.nombre')
		->first();
		
 
		$datos=[
			'correlativo' => $correlativo,
			'movimientos' => $movimientos,
			'afacturar'   => $afacturar
			];
			return $datos;
	}

	public function getAgrupar(Request $request){
	
		$id=$request->id;

		DB::beginTransaction();
			try {
				$rs=afacturar::select(DB::RAW('COUNT(afacturar.id) as id'))->where('sucursal_id', '=', $id)->first();	
					if($rs->id > 0){
						afacturar::where('sucursal_id', '=', $id)->delete();
					
					}
						DB::select('select * from agrupar(:id)', ['id' => $id]);				 
				}
			 catch (Exception $e) {
				DB::rollback();
				return $e->errorInfo[2];
			}
			DB::commit();
			return ['s' => 's', 'msj' => 'Operacion exitosa'];
	}

	public function getCorrelativo(Request $request)
	{
		$id = $request->id;
	
	 DB::beginTransaction();
		try {
				$rs = sucursal::find($id);
				$actual  =  $rs['correlativo'];

				$facturas=afacturar::select('id','fecha')->where('sucursal_id', '=', $id)->orderBy('fecha', 'asc')->get()->toArray();

				foreach ($facturas as $factura) {		

					afacturar::find($factura['id'])->update([
						'correlativo' =>  $actual = $actual + 1,
					]);
				}

				sucursal::find($id)->update([
						'correlativo' =>  $actual,
					]);
			}
		 catch (Exception $e) {
			DB::rollback();
			return $e->errorInfo[2];
		}
		DB::commit();
		return ['s' => 's', 'msj' => 'Operacion exitosa'];
	}
}

