<?php namespace Modules\Facturacion\Http\Controllers;

use DB;
use Carbon\Carbon;
use Modules\Facturacion\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Excel; 

use Modules\Facturacion\Model\definiciones\sucursal;
use Modules\Facturacion\Model\controlfacturacion;
use Modules\Facturacion\Model\movimientos;
use Modules\Facturacion\Model\pendientes;




class ReporteController extends Controller {
	
	protected $titulo = 'Reporte';

	
	public function getImprimir(){

		$pdf = new \mPDF();

		$query = DB::table('movimientos')
		->select(DB::RAW('SUM(movimientos.monto) as monto'),'movimientos.banco','sucursal.nombre as sucursal')
		->join('clientes', 'movimientos.ci', '=', 'clientes.ci')
		->join('sucursal', 'clientes.sucursal_id', '=', 'sucursal.id')
		->whereIn('movimientos.estatus', [0, 3])
		->groupBy('movimientos.banco','sucursal.nombre')
		->orderBy('movimientos.banco', 'asc');
	

	
		$bancos = DB::table('movimientos')
		->select('movimientos.banco')
		->whereIn('movimientos.estatus', [0, 3])
	 	->orderBy('movimientos.banco', 'asc')
		->groupBy('movimientos.banco');
		


		$cantidades  = [];

		$total = 0;
		$totalbanco = [];
		$totalsucursal = [];

		foreach ($query->get() as $movimiento) {
			$cantidades[$movimiento->banco][$movimiento->sucursal] = $movimiento->monto;

			$total += $movimiento->monto;

			//Acumulador de Totales de cantidad de alimento por Rubros
			if (!isset($totalbanco[$movimiento->banco])){
				$totalbanco[$movimiento->banco] = 0;
			}

			$totalbanco[$movimiento->banco] += $movimiento->monto;
			
			
			//Acumulador de Totales de cantidad de alimento por empresas
			if (!isset($totalsucursal[$movimiento->sucursal])){
				$totalsucursal[$movimiento->sucursal] = 0;
			}
			$totalsucursal[$movimiento->sucursal] += $movimiento->monto;
		}

		//dd($totalsucursal);
		//pasa a la vista las consulta
		$rs = controlfacturacion::where('estatus', '=', 1)->first();
		$total_movimientos = $rs['total_bolivares'];

		$total_rechazados = DB::table('movimientos')
	 			->select(DB::raw('SUM(monto) as total'))
	 			->where('estatus', '=', '1')
				->first();

		$total_pendientes = DB::table('pendientes')
	 			->select(DB::raw('SUM(monto) as total'))
				->first();

				$totalafacturar = $total_movimientos - $total_rechazados->total + $total_pendientes - $totalsucursal["Puerto la cruz"];

		$pdf->WriteHTML(view('facturacion::pdf.reporte', [
			'bancos'     	   =>  $bancos->get(),
			'sucursales' 	   =>  sucursal::all(),
			'candidades' 	   =>  $cantidades,
			'totalbanco' 	   =>  $totalbanco,
			'totalsucursale'   =>  $totalsucursal,
			'total'			   =>  $total,
			'totalmovimientos' =>  $total_movimientos,
			'totalrechazados'  =>  $total_rechazados,
			'totalpendientes'  =>  $total_pendientes,
			'totalafacturar'   =>  $totalafacturar
		])->render());

		$pdf->Output();
	}

	public function getExcelmovimientos()
	{
	

		Excel::create('movimientos Excel', function($excel) {

            $excel->sheet('movimentos vinculados', function($sheet) {

	         
            	$query = movimientos::select('movimientos.ci','movimientos.fecha','movimientos.monto', 'movimientos.banco','sucursal.abreviatura as sucursal')
				->join('clientes', 'movimientos.ci', '=', 'clientes.ci')
				->join('sucursal', 'clientes.sucursal_id', '=', 'sucursal.id')
				->whereIn('movimientos.estatus', [0, 3])
				->orderBy('movimientos.banco', 'asc');
					
					
                $sheet->fromArray($query->get());

            });

        })->export('xls');	
	}
	public function getExcelrechazado()
	{
	
		Excel::create('rechazado Excel', function($excel) {

            $excel->sheet('movimentos rechazados', function($sheet) {

	         
            	$query = movimientos::select('movimientos.ci','movimientos.fecha','movimientos.monto', 'movimientos.banco')
				->where('movimientos.estatus','=', 1)
				->orderBy('movimientos.banco', 'asc');
					
					
                $sheet->fromArray($query->get());

            });

        })->export('xls');	
	}

	public function getExcelpendientes()
	{
	
		Excel::create('rechazado Excel', function($excel) {

            $excel->sheet('movimentos rechazados', function($sheet) {

	         
            	$query = pendientes::select('pendientes.ci','pendientes.fecha','pendientes.monto', 'pendientes.banco')
				->where('pendientes.estatus','=', 1)
				->orderBy('pendientes.banco', 'asc');
					
					
                $sheet->fromArray($query->get());

            });

        })->export('xls');	
	}
}  