<?php namespace Modules\Facturacion\Http\Controllers;
  
use DB;
use Illuminate\Http\Request;

use Yajra\Datatables\Datatables;
use Excel; 

use Modules\Facturacion\Http\Controllers\Controller;

use Carbon\Carbon;

use Modules\Facturacion\Model\controlfacturacion as modelo;
use Modules\Facturacion\Model\movimientos;

use Modules\Facturacion\Http\Requests\controlfacturacionRequest as facturacion_request;


class FacturacionController extends Controller {
	
	public $titulo = 'Nueva Facturacion';
	
	public $librerias = [
		'ladda',
	];

	public $js=[

	];
	public $meses = [		
		1 =>'Enero',
		'Febrero', 
		'Marzo',
		'Abril',
		'Mayo',				 
		'Junio',
		'Julio',
		'Agosto',
		'Septiembre',
		'Octubre',
		'Noviembre',
		'Diciembre',
	];
	 
	public function getIndex()
	{
		
		//$this->resulcarga(6);
		$ultimaFecha = DB::table('clientes')->select(DB::raw('max(created_at) as fecha'))->first();		
		return $this->view('facturacion::facturacion', [
			'ultimaFecha' => Carbon::parse($ultimaFecha->fecha)->format('d/m/Y'),
		]);
		
	}

	
	public function verificar()
	{
	 	$cantidad = DB::table('controlfacturacion')
	 			->select('estatus')
	 			->where('estatus', '=', '1')
				->count();

	 	if($cantidad > 0){
	 		return 1; //activo
	 	}
	 	return 0; //no activo
	}

	public  function getResulcarga(Request $request)
	{

	 $query = DB::table('movimientos')
			->select(DB::raw('SUM(monto) as total'),DB::raw('count(controlfacturacion_id) as movimientos'))
			->where('estatus', '=', '1')
			->where('controlfacturacion_id', '=', $request->id)
			->first();

			 modelo::find($request->id)->update([
			'total_registros' =>  $query->movimientos,
			'total_bolivares' =>  $query->total
			]);

			 return (array) $query;
	}

	public function postCrear(facturacion_request $request){
		set_time_limit(0);
		DB::beginTransaction();
		try {
			
			//dd($request->all());
			if($this->verificar() == 1){
				return ['s' => 'n', 'msj' => 'Aviso: No se puede crear  un nuevo proceso de facturacion, ya que existe uno en curso'];
			}

			$estatus=1;
			//$fecha2= Carbon::parse($request->fecha_update_client)->format("Y-m-d");
			$rs = modelo::create([
				'fecha_inicio'			=> date('Y-m-d'),
				'fecha_update_client'	=>date('Y-m-d'),
				'mes'					=>$request->mes,
				'ano'					=>$request->ano,
				'estatus'				=>$estatus,
				]);
			
			$id = $rs['id'];

			$mimes = [
			'text/csv',
			'application/vnd.ms-excel',
			'application/vnd.oasis.opendocument.spreadsheet',
			'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
			];

			$ruta = public_path('archivos/');
			$archivo = $request->file('subir');

			$mime = $archivo->getClientMimeType();

			/*if (!in_array($mime, $mimes)) {
				return "Error de Validación, la extension el archivo cargado debe ser: xls,xlsx,csv";
			}*/
			
			do {
				$nombre_archivo = str_random(20) . '.' . $archivo->getClientOriginalExtension();
			} while (is_file($ruta . $nombre_archivo));
				
			$archivo->move($ruta, $nombre_archivo);

			chmod($ruta . $nombre_archivo, 0777);

			$excel = Excel::selectSheetsByIndex(0)->load($ruta . $nombre_archivo)->get();
			//dd($excel);
			$estatus=1;

	 		foreach ($excel as $key => $value) {
	 				
	 				$fecha= Carbon::parse($value->fecha)->format("Y-m-d");

					$cantidad = floatval(str_replace(',', '.', $value->monto));
				
	 				movimientos::create([
	 					'ci'					=> 	$value->cedula,
						'fecha'					=>	$fecha,
						'monto'					=>	$cantidad,
						'banco'					=>	$value->banco,
						'estatus'				=>	$estatus,
						'controlfacturacion_id'	=>	$id
					]);
	 			}	

		} catch (Exception $e) {
			DB::rollback();
			return $e->errorInfo[2];
		}


		DB::commit();
		return ['s' => 's', 'msj' => trans('controller.incluir'), 'id' => $id];
	}

}  
