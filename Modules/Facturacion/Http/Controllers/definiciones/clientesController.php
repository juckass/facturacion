<?php namespace Modules\Facturacion\Http\Controllers\definiciones;
  
use DB;
use Illuminate\Http\Request;

use Yajra\Datatables\Datatables;

use Modules\Facturacion\Http\Controllers\Controller;

use Modules\Facturacion\Model\clientes as modelo;
use Modules\Facturacion\Model\definiciones\sucursal ;

use Modules\Facturacion\Http\Requests\clientesRequest as Clientes_request;


class clientesController extends Controller {
	
	public $titulo = 'Clientes';
	
	public $librerias = [
		
		'datatables', 
		
	];

	public $js=[

		'definiciones/clientes'
	];

	public function getIndex()
	{
		return view('facturacion::clientes', $this->_app());
	}
	
	public function getBuscar(Request $request, $id = 0) {
		$rs = modelo::find($id);

		if ($rs) {
			return array_merge($rs->toArray(), [
				's' => 's',
				'msj' => trans('controller.buscar'),
			]);
		}

		return trans('controller.nobuscar');
	}

	
	public function postCrear(Clientes_request $request){

		DB::beginTransaction();
		try {
			$rs = modelo::create($request->all());

		} catch (Exception $e) {
			DB::rollback();
			return $e->errorInfo[2];
		}

		DB::commit();
		return ['s' => 's', 'msj' => trans('controller.incluir')];
	}

	public function putActualizar(Request $request, $id = 0) {
		DB::beginTransaction();
		try {
			//dd($request->all())
			$rs = modelo::find($id)->update($request->all());
		} catch (Exception $e) {
			DB::rollback();
			return $e->errorInfo[2];
		}

		DB::commit();
		return ['s' => 's', 'msj' => trans('controller.incluir')];
	}

	public function deleteEliminar(Request $request, $id = 0) {
		try {
			$rs = modelo::destroy(intval($id));
		} catch (Exception $e) {
			return $e->errorInfo[2];
		}

		return ['s' => 's', 'msj' => trans('controller.eliminar')];
	}

	
	public function getDatatable() {
		$sql = modelo::select('clientes.id as id','clientes.ci as cedula', 'clientes.nombre as cliente', 'sucursal.nombre as sucursal ')
				->leftJoin('sucursal', 'clientes.sucursal_id', '=', 'sucursal.id');
		return Datatables::of($sql)->setRowId('id')->make(true);
	}

	public function sucursales() {
		return sucursal::lists('nombre', 'id');
	}
}  
