<?php namespace Modules\Facturacion\Http\Controllers;
  
use DB;
use Illuminate\Http\Request;

use Modules\Facturacion\Http\Controllers\Controller;
use Modules\Facturacion\Model\controlfacturacion;
use Modules\Facturacion\Model\libro;
use Modules\Facturacion\Model\movimientos;
use Modules\Facturacion\Model\afacturar;

use Excel;
use Carbon\Carbon;


class CierreFacturacionController extends Controller {
	
	protected $titulo = 'Cierre';
	
	protected $id_control;
	public $js=[
	 'cierre'
	];

	public function __construct()
	{
		parent::__construct();
		
		$rs = controlfacturacion::where('estatus', '=', 1)->first();
		$this->id_control = $rs['id'];
	}

	public function getIndex()
	{
		return $this->view('facturacion::cierre',[
			'control' => $this->verificar(),
		]);
	}

	public function verificar()
	{
	 	$cantidad = DB::table('controlfacturacion')
	 			->select('estatus')
	 			->where('estatus', '=', '1')
				->count();

	 	if($cantidad > 0){
	 		return 1; //activo
	 	}
	 	return 0; //no activo
	}

	public function getCierre(){	

	DB::beginTransaction();
		try {

			$total_pendientes = DB::table('pendientes')
 			->select(DB::raw('count(id) as id'))
			->first();		

			$total_facturado = DB::table('afacturar')
 			->select(DB::raw('sum(total) as total'))
			->first();		
			
			DB::table('libro')->truncate();
			DB::table('movimientos')->truncate();
			DB::table('afacturar')->truncate();
			DB::table('controlfacturacion')->truncate();
	
		}
		catch (Exception $e) {
			DB::rollback();
			return $e->errorInfo[2];
		}
		DB::commit();
		return ['s' => 's', 'msj' => 'Operacion exitosa'];
	}
}
