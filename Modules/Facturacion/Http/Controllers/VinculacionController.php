<?php namespace Modules\Facturacion\Http\Controllers;
  
use DB;
use Illuminate\Http\Request;
use Modules\Facturacion\Http\Requests\movimientoRequest;

use Yajra\Datatables\Datatables;
use Carbon\Carbon;

use Modules\Facturacion\Http\Controllers\Controller;

use Modules\Facturacion\Model\clientes as modelo;
use Modules\Facturacion\Model\movimientos;
use Modules\Facturacion\Model\controlfacturacion;
use Modules\Facturacion\Model\pendientes;


class VinculacionController extends Controller {
	
	protected $titulo = 'Vincular';
	
	public $librerias = [
		'jquery-ui',
		'jquery-ui-timepicker', 
		'datatables', 
		'ladda'
	];
	
	protected $id_control;

	public function __construct()
	{
		parent::__construct();
		
		$rs = controlfacturacion::where('estatus', '=', 1)->first();
		$this->id_control = $rs['id'];
	}

	public function getIndex()
	{
		return $this->view('facturacion::vincular',[
			'control' =>$this->verificar(),
			]);
	}

	public function verificar()
	{
	 	$cantidad = DB::table('controlfacturacion')
	 			->select('estatus')
	 			->where('estatus', '=', '1')
				->count();

	 	if($cantidad > 0){
	 		return 1; //activo
	 	}
	 	return 0; //no activo
	}
//Informacion
	public function getVinculados()
	{
		$vinculados = DB::table('movimientos')
	 			->select(DB::raw('count(estatus) as estatus'),DB::raw('SUM(monto) as total'))
	 			->where('estatus', '=', '0')
				->first();

		$pendientes = DB::table('movimientos')
	 			->select(DB::raw('count(estatus) as estatus'),DB::raw('SUM(monto) as total'))
	 			->where('estatus', '=', '3')
				->first();

		$datos = [
			'vinculados' => $vinculados,
			'pendientes' => $pendientes
		];
				
	 	if($vinculados->estatus > 0){
	 		return  $datos;
	 		//(array) $cantidad; 
	 	}
	 	return 0;
	}

	public function getRechazados()
	{
		$cantidad = DB::table('movimientos')
	 			->select(DB::raw('count(estatus) as estatus'),DB::raw('SUM(monto) as total'))
	 			->where('estatus', '=', '1')
				->first();

	 	if($cantidad->estatus > 0){
	 		return (array) $cantidad; 
	 	}
	 	return 0;
	}

	public function getPendientes()
	{
		$cantidad = DB::table('pendientes')
	 			->select(DB::raw('count(id) as id'),DB::raw('SUM(monto) as total'))
				->first();
				
	 	if($cantidad->id > 0){
	 		return (array) $cantidad; 
	 	}
	 	return 0;	
	}

	//Autos vinculaciones
		//vincula todos los rechazados
	public function getAutovincular()
	{		
		DB::beginTransaction();
		try {
			DB::table('movimientos')
			->join('clientes', 'movimientos.ci', '=', 'clientes.ci')
			->where('estatus', '=', 1)
			->update(array('estatus' => 0));
			
			$this->control(); 

			}
		 catch (Exception $e) {
			DB::rollback();
			return $e->errorInfo[2];
		}
		DB::commit();
		return ['s' => 's', 'msj' => 'Vinculación exitosa'];
	}

		//auto-vincula todo los pendientes
	public  function postVinculadospedientes(Request $request)
	{
		DB::beginTransaction();
		try {
				$query= DB::table('pendientes')
				->select('pendientes.id','pendientes.ci','pendientes.fecha','pendientes.monto','pendientes.banco','pendientes.estatus')
				->join('clientes', 'pendientes.ci', '=', 'clientes.ci')
				->where('pendientes.estatus', '=', 1)
				->get();

				$id = $this->id_control;

				$fecha = $request->fecha;
				$fecha_format = Carbon::createFromFormat('d/m/Y', $fecha)->format("Y-m-d");

				foreach ($query as $value) {
					
					$invert = explode("-", $value->fecha);
					$mes2   = $invert[1];
					$banco = $value->banco."-".$mes2;

					movimientos::create([
			
						'ci' 					=> $value->ci,
						'fecha' 				=> $fecha_format,
						'monto'					=> $value->monto,
						'banco' 				=> $banco,
						'estatus' 				=> 3,
						'controlfacturacion_id' => $id

					]);

					$user = pendientes::find($value->id);
					$user->delete();
					$this->control();	
				}
			
			}
		 catch (Exception $e) {
			DB::rollback();
			return $e->errorInfo[2];
		}

		DB::commit();
		return ['s' => 's', 'msj' => 'Vinculación exitosa'];	
	}

	//vinculaciones mmanual
	public function postActualizar(movimientoRequest $request){
		$id_control = $this->id_control;
		DB::beginTransaction();
		try {
			   
			$cantidad = DB::table('clientes')
 			->select(DB::raw('count(ci) as ci'))
 			->where('ci',  '=', $request->ci)
			->first();        
				
				if($cantidad->ci > 0){

					switch ($request->opera) {
						case 'pendiete':
							
							$invert = explode("/", $request->fecha);
							$mes2   = $invert[1];
							$banco = $request->banco."-".$mes2;
							
								$rs = pendientes::find($request->id);
								 if($rs['fecha']==$request->fecha){
								 	return ['s' => 'n', 'msj' => 'Debe Seleccinar el primer dia habil del mes que esta facturando'];
								 }


								 
							movimientos::create([
								'ci' 					=> $request->ci,
								'fecha' 				=> Carbon::parse($request->fecha)->format("Y-m-d"),
								'monto'					=> $request->monto,
								'banco' 				=> $banco,
								'estatus' 				=> 3,
								'controlfacturacion_id' => $id_control
							]);
							$pendiete = pendientes::find($value->id);
							$pendiete->delete();
					
							break;
						case 'rechazado':
							
							movimientos::find($request->id)->update([
								'ci'		=>$request->ci,
								'fecha'		=>Carbon::parse($request->fecha)->format("Y-m-d"),
								'monto'		=>$request->monto,
								'banco'		=>$request->banco,
								'estatus'	=>0
							]);	
			
							break;

						case 'vinculado':

							movimientos::find($request->id)->update([
								'ci'		=>$request->ci,
								'fecha'		=>$request->fecha,
								'monto'		=>$request->monto,
								'banco'		=>$request->banco,
								'estatus'	=>0
							]);	

							break;	
					}

					$this->control();
				} else{
					return ['s' => 'n', 'msj' => 'El cliente no existe'];	 
				}
			}

		 catch (Exception $e) {
			DB::rollback();
			return $e->errorInfo[2];
		}

		DB::commit();
		return ['s' => 's', 'msj' => 'Vinculación exitosa'];	
	}
	
	//busquedas
		//busca rechazado
	public	function getBuscarregistro(Request $request){
	
		$id = $request->id;

		switch ($request->opera) {
			case 'pendiete':
				$rs = pendientes::find($id);
				break;
			case 'rechazado':
				$rs = movimientos::find($id);
				break;
		}
		


		if ($rs) {
			return array_merge($rs->toArray(), [
				's' => 's',
				'msj' => trans('controller.buscar'),
			]);
		}

		return trans('controller.nobuscar');
	}

	public  function getBuscarvinculados(Request $request)
	{
		$id = $request->id;	
	

		$query = movimientos::select('movimientos.id','movimientos.ci','movimientos.monto','movimientos.banco','movimientos.fecha','sucursal.nombre as sucursal', 'clientes.nombre as cliente')
		->join('clientes', 'movimientos.ci', '=', 'clientes.ci')
		->join('sucursal', 'clientes.sucursal_id', '=', 'sucursal.id')
		->where('movimientos.id', '=', $id);

		$rs =$query->first();

		if ($rs) {
			return array_merge($rs->toArray(), [
				's' => 's',
				'msj' => trans('controller.buscar'),
			]);
		}

		return trans('controller.nobuscar');

	}

	//data tables
	
	public function getDatatable() {
	$sql = movimientos::select('id','ci','monto', 'fecha', 'banco')->where('estatus', '=', 1);
		return Datatables::of($sql)->setRowId('id')->make(true);
	}

	public function getDatatablependientes() {
	$sql = pendientes::select('id','ci','monto', 'fecha', 'banco');
		return Datatables::of($sql)->setRowId('id')->make(true);
	}

	public function getDatatablevinculados() {
	$sql = movimientos::select('id','ci','monto', 'fecha', 'banco')->where('estatus', '=', 0);		
		return Datatables::of($sql)->setRowId('id')->make(true);
	}

	//funciones

	protected function control(){

		$id=$this->id_control;
		$datos = $this->getVinculados();
		$datos = $this->getVinculados();
 		$vinculados = $datos['vinculados']->estatus;
		
		$query = DB::table('movimientos')
			->select(DB::raw('count(controlfacturacion_id) as movimientos'))
			->where('controlfacturacion_id', '=', $id)
			->first();

		$query2 = DB::table('movimientos')
			->select(DB::raw('SUM(monto) as total'))
			->whereIn('movimientos.estatus', [0, 1])
			->where('controlfacturacion_id', '=', $id)
			->first();	
	
		controlfacturacion::find($id)->update([
			'total_vinculados' => $vinculados,
			'total_registros' =>  $query->movimientos,
			'total_bolivares' =>  $query2->total
			]);
	}
	//eliminar
	public function deleteEliminar(Request $request) {
		try {
			
			$id = $request->id;	
			switch ($request->op) {
			case 'pendiete':
				$user = pendientes::find($id);
				$user->delete();
				break;

			case 'rechazado':
				$user = movimientos::find($id);
				$user->delete();
				$this->control();
				break;

			case 'vinculado':
				$user = movimientos::find($id);
				$user->delete();
				$this->control();
				break;	
			}
			
		} catch (Exception $e) {
			return $e->errorInfo[2];
		}

		return ['s' => 's', 'msj' => trans('controller.eliminar')];
	}

}  
		