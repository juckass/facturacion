<?php namespace Modules\Facturacion\Http\Controllers;
  
use DB;
use Illuminate\Http\Request;

use Modules\Facturacion\Http\Controllers\Controller;

use Carbon\Carbon;




class TxtController extends Controller {
	

	public $meses = [		
		1 => 'Enero',
		'Febrero', 
		'Marzo',
		'Abril',
		'Mayo',				 
		'Junio',
		'Julio',
		'Agosto',
		'Septiembre',
		'Octubre',
		'Noviembre',
		'Diciembre',
	];

	public function postFacturas(Request $request)
	{
		mb_internal_encoding('UTF-8');
		$id =$request->id;
	 	$cantidad = DB::table('controlfacturacion')
			->select('mes')
			->where('estatus', '=', '1')
		->first();
		$mese = $this->meses[$cantidad->mes];

		$movimientos = DB::table('afacturar')
		->select( 'clientes.nacionalidad',
				  'clientes.ci', 
				  'clientes.nombre',
				  'clientes.direccion',
				  'clientes.estado',
				  'clientes.ciudad',
				  'afacturar.total',
				  'afacturar.fecha',
				  'afacturar.correlativo'	)
		->join('clientes', 'afacturar.ci', '=', 'clientes.ci')
		->join('sucursal', 'clientes.sucursal_id', '=', 'sucursal.id')
		->where('sucursal.id', '=', $id)
		->orderBy('correlativo', 'asc')
		->get();
		switch ($id) {
			case 1:
				$con= "";
				$sucur= 'bolivar';  
				break;
			case 2:
				$con="SERIE B";
				$sucur= 'Puerto-ordaz';  
				break;
			case 3:
				$con="SERIE C";
				$sucur= 'Tigre'; 
				break;
			}
		header("Content-type: text/plain");
		header("Content-Disposition: attachment; filename=1-".$sucur."-".$mese.".txt");
		header("Pragma: no-cache");
		header("Expires: 0");

		foreach ($movimientos as $movimiento) {
			
			switch ($id) {
				case 1:
					echo "\n";
					echo "\n";
					echo "\n";
					echo "\n";
					echo "\n";
					echo "\n";
					break;
				case 2:
					echo "\n";
					echo "\n";
					echo "SUCURSAL: CALLE CUCHIVERO TORRE \n";
					echo "CONTINENTAL NIVEL MEZZANINA LOCAL 15\n";
					echo "PTO ORDAZ - ESTADO BOLIVAR\n";
					echo "\n";
					break;
				case 3:
					echo "\n";
					echo "\n";
					echo "CENTRO EMPRESARIAL IRPINIA PISO \n";
					echo "AVENIDA WINSTON CHURCHILL\n";
					echo "EL TIGRE, ESTADO ANZOATEGUI\n";
					echo "\n";
					break;
			}

			echo '         CONTRIBUYENTE FORMAL';
			echo "\n";
			echo '    FECHA             '.$con.' NRO. FACTURA';
			echo "\n  ";
			echo date('d/m/Y', strtotime($movimiento->fecha));
			echo "                ";
			echo $movimiento->correlativo;
			echo "\n";
			echo "CLIENTE: ".strtoupper($movimiento->nombre);
			echo "\n";
			echo "R.I.F: ".strtoupper($movimiento->nacionalidad).'-'.$movimiento->ci;
			echo "\n";
			echo "DIRECCION: ".strtoupper(substr($movimiento->direccion, 0, 28));
			echo "\n";
			echo substr(strtoupper($movimiento->direccion), 28, 39);
			
			echo "\n";
			echo "CIUDAD: ";
			switch ($id) {
				case 1:
					echo "BOLÍVAR";
					break;
				case 2:
					echo "GUAYANA";
					break;
				case 3:
					echo "EL TIGRE";
					break;
			}
			echo "\n";
			echo str_repeat("-", 39);
			echo "\n";
			echo str_pad("CONCEPTO", 39, " ", STR_PAD_BOTH);
			echo "\n";
			echo str_repeat("-", 39);
			echo "\n";
			echo "\n";
			echo "PAGO DE CONSULTAS MEDICAS Y ODONTOLOGI-\n";
			echo "CAS CORRESPONDIENTES AL MES DE\n";
			echo "$mese AÑO ".date('Y') ." (E)\n";
			echo "\n";
			echo str_repeat("-", 39);
			echo "\n";
			echo "SUB-TOTAL: ";
			echo str_pad(strval($movimiento->total), 28, " ", STR_PAD_LEFT);
			echo "\n";
			echo "EXENTO   :";
			echo str_pad(strval($movimiento->total), 29, " ", STR_PAD_LEFT);
			echo "\n";
			echo "I.V.A.   :                         0.00";
			echo "\n";
			echo "TOTAL    :";
			echo str_pad(strval($movimiento->total), 29, " ", STR_PAD_LEFT);
			echo "\n";
			echo "\n";
			echo "\n";
			echo "\n";
			echo "\n";
		};	
			
	}

}  
