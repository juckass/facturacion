<?php namespace Modules\Facturacion\Http\Requests;

use App\Http\Requests\Request;

class SucursalRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			
			'correlativo'   	=> ['required', 'integer' ],
			'cod_sucursal'  => ['required', 'integer' ],
			'nombre'   		=> ['required', 'min:3', 'max:50'],
		];
	}

}
