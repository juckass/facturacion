<?php

namespace Modules\Facturacion\Http\Requests;

use App\Http\Requests\Request;
 
class movimientoRequest extends Request {
	protected $tabla = 'movimiento';
	protected $rules = [
		'ci'			=> ['required', 'integer'], 
		'monto'			=> ['required', 'regex:/^\d{1,8}([\.\,]\d{1,3})?$/'], 
		'fecha'		    => ['required', 'date_format:d/m/Y'],
		'banco'		    => ['required', 'min:3', 'max:80'], 	
	];

	public function rules(){
		return $this->reglas();
	}
}

