<?php namespace Modules\Facturacion\Http\Requests;

use App\Http\Requests\Request;

class controlfacturacionRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			
			'mes'   			   => ['required', 'integer' ],
			'ano'  				   => ['required', 'integer' ],
			'fecha_update_client'  => ['required', 'min:3', 'max:50'],
			'subir'  			   => ['required']


		];
	}

}
