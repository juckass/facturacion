<?php

namespace Modules\Facturacion\Http\Requests;

use App\Http\Requests\Request;
 
class clientesRequest extends Request {
	protected $tabla = 'clientes';
	protected $rules = [
	
		'nacionalidad'  => ['required', 'min:1', 'max:2'], 
		'ci'			=> ['required', 'integer'], 
		'nombre'		=> ['required', 'min:3', 'max:200'], 
		'direccion'		=> ['required', 'min:3', 'max:80'], 
		'estado'		=> ['required', 'min:3', 'max:80'], 
		'ciudad'		=> ['required', 'min:3', 'max:80'], 
		'sucursal_id' 	=> ['required', 'integer']
	];

	public function rules(){
		return $this->reglas();
	}
}

