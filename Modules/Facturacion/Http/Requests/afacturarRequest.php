<?php

namespace Modules\Facturacion\Http\Requests;

use App\Http\Requests\Request;
 
class afacturarRequest extends Request {
	protected $tabla = 'afacturar';
	protected $rules = [
		'ci' => ['required', 'integer'], 
		'fecha' => ['required'], 
		'total' => ['required'], 
		'banco' => ['required', 'min:3', 'max:255'], 
		'estatus' => ['required', 'min:3', 'max:2'], 
		'totalmovi' => ['required', 'integer'], 
		'sucursal_id' => ['required', 'integer'], 
		'controlfacturacion_id' => ['required', 'integer']
	];

	public function rules(){
		return $this->reglas();
	}
}