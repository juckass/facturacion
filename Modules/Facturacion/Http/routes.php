<?php

Route::group(['middleware' => 'web', 'prefix' => Config::get('admin.prefix'), 'namespace' => 'Modules\Facturacion\Http\Controllers'], function()
{
	//Route::get('/', 'FacturacionController@index');

	Route::controllers([
		'facturacion/definiciones/sucursal'			=> 'definiciones\SucursalesController',
		'facturacion/definiciones/clientes'			=> 'definiciones\clientesController',
		'facturacion/carga'					        => 'ClientesController',
		'facturacion/facturacion'					=> 'FacturacionController',
		'facturacion/vincular'						=> 'VinculacionController',
		'facturacion/reporte'						=> 'ReporteController',
		'facturacion/afacturar'						=> 'AfacturarController',
		'facturacion/txt'							=> 'TxtController',
		'facturacion/libro'							=> 'libroController',
		'facturacion/cierre'						=> 'CierreFacturacionController'
	]);
});   