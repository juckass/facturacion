 var aplicacion, aplicacion2, $form, $form2, $form3, tabla, tabla2, tabla3;
$(function(){
	//definiciones
	aplicacion = new app("formulario", {
		'botones' : false,
		'limpiar' : function(){
			tabla.fnDraw();
		}
	});

	aplicacion2 = new app("formulario2", {
		'botones' : false,
		'teclado' : false,
		'limpiar' : function(){
			tabla2.fnDraw();
		} 
	});

	aplicacion3 = new app("formulario3", {
		'botones' : false,
		'teclado' : false,
		'limpiar' : function(){
			tabla3.fnDraw();
		}
	});


	iniciar();

	$form = aplicacion.form;
	$form2 = aplicacion2.form;
	$form3 = aplicacion3.form;

	//eventos 

	//muestra data table rechazados
	$('#rechazadosmanual').on('click', function(){
		$('#form3').hide();
		$('#modalTablaBusqueda').modal('show');
	});

	$('#pendientemanual').on('click', function(){
		$('#form2').hide();
		$('#lispendiente').modal('show');
	});

	$('#movimientos').on('click', function(){
		$('#form2').hide();
		$('#lisvinculados').modal('show');
	});

	$('#eliminar').on('click', function(){

		$id=$('#id').val();
		$op=$('#opera').val();
		Eliminar($id,$op);
	});

	$('#eliminarmovi').on('click', function(){
		$id=$('#id_2').val();
		$op=$('#opera2').val();
		Eliminar($id,$op);
	});

	//click auto  vincular rechazados
	$("#auto-vincular-r").on('click', function(){	
		autovincular();
	});

	//muestra formulario fecha de rechazado	
	$("#ver-p").on('click', function(){		
		$("#form1").toggle('slow');
	});

	//Vinculacion Automarico Pendientes
	$("#cargapendiente").on('click', function(){
		var l = Ladda.create($("#cargapendiente").get(0));
	 	l.start();

		var options2 = { 
			url : $url + 'vinculadospedientes',
			type : 'POST',
			data : $("#formulario").serialize(),
			success: function(r){			
				aviso(r);
				//resul_carga(r.id);
				iniciar();

			},
			complete : function(x,e,o){
				ajaxComplete(x,e,o);
				l.stop();
				iniciar();		
			}
		}; 
		
		$('#formulario').ajaxSubmit(options2); 
	});

	//vinculacion manual envio de datos a actualizar Movimientos Rechezados
	$("#manual").on('click', function(){
		var options2 = { 
			url : $url + 'actualizar', 
			type : 'POST',
			data : $("#formulario2").serialize(),
			success: function(r){
				if (r.s == 'n'){
					aviso(r);
					return;
				}

				aviso(r);
				iniciar();
				$('#form2').hide();
				aplicacion2.limpiar();
				tabla.fnDraw();
				tabla2.fnDraw();
			},
			complete : function(x,e,o){
				ajaxComplete(x,e,o);
				iniciar();	
				
						
			}
		}; 
		
		$('#formulario2').ajaxSubmit(options2); 
	});

	$("#movi").on('click', function(){
		$('#form2').hide();
		var options2 = { 
			url : $url + 'actualizar', 
			type : 'POST',
			data : $("#formulario3").serialize(),
			success: function(r){
				if (r.s == 'n'){
					aviso(r);
					return;
				}

				aviso(r);
				iniciar();
				$('#form3').hide();
				aplicacion3.limpiar();
				tabla.fnDraw();
				tabla2.fnDraw();
				tabla3.fnDraw();
			},
			complete : function(x,e,o){
				ajaxComplete(x,e,o);
				iniciar();				
			}
		}; 
		
		$('#formulario3').ajaxSubmit(options2); 
	});


	// widget
	$("#fecha2", $form).datepicker();
	$("#fecha", $form2).datepicker();
	$("#fecha3", $form3).datepicker();

	//data tabla de movimientos rechazados
	tabla = $('#tabla')
	.on("click", "tbody tr", function(){
		BuscarRegistro(this.id,'rechazado');
	})
	.dataTable({
		processing: true,
		serverSide: true,
		ajax: $url + "datatable",
		oLanguage: datatableEspanol,
		columns: [
			{ data: 'ci', 		name: 'ci'},
			{ data: 'monto',	name: 'monto'},
			{ data: 'banco',	name: 'banco'},
			{ data: 'fecha', 	name: 'fecha'}
		]
	});

	tabla2 = $('#tablaPendientes')
	.on("click", "tbody tr", function(){
		//aplicacion.buscar(this.id);

		BuscarRegistro(this.id,'pendiete');	
	})
	.dataTable({
		processing: true,
		serverSide: true,
		ajax: $url + "datatablependientes",
		oLanguage: datatableEspanol,
		columns: [
			{ data: 'ci', 		name: 'ci'},
			{ data: 'monto',	name: 'monto'},
			{ data: 'banco',	name: 'banco'},
			{ data: 'fecha', 	name: 'fecha'}	
		]
	});

	tabla3 = $('#tablavinculados')
	.on("click", "tbody tr", function(){
		//aplicacion.buscar(this.id);
		Buscarvincuado(this.id);	
	})
	.dataTable({
		processing: true,
		serverSide: true,
		ajax: $url + "datatablevinculados",
		oLanguage: datatableEspanol,
		columns: [
			{ data: 'ci', 		name: 'ci'},
			{ data: 'monto',	name: 'monto'},
			{ data: 'banco',	name: 'banco'},
			{ data: 'fecha', 	name: 'fecha'}	
		]
	});

});

function iniciar(){
	rechazados();
	vinculados();
	pendientes();
}

//verifica pendientes
function pendientes(){
	$.ajax({
		url : $url + 'pendientes',
		success : function(r){
			//console.log(r);
		if (r.id > 0){
				$('#pendiente_estatus').text(r.id);
				$('#pendiente_total').text(r.total);
				$('#pendientes').show(1000);
			}else{

				$('#pendientes').hide();
				$('#form1').hide();
			}
		}
	});
}

//verifica rechazados
function rechazados(){
	$.ajax({
		url : $url + 'rechazados',
		success : function(r){
			//console.log(r.estatus);
			if (r.estatus > 0){
				$('#rechazados_estatus').text(r.estatus);
				$('#rechazados_total').text(r.total);
				$('#rechazados').show(1000);
			}else{

				$('#rechazados').hide();
			}
		}
	});
}

//verifica vinculados 
function vinculados(){
	$.ajax({
		url : $url + 'vinculados',
		success : function(r){
			//console.log(r.vinculados['total']);
			if (r.vinculados['estatus'] > 0){	
				$('#vinculados_estatus').text(r.vinculados['estatus']);
				$('#pendientes_total').text(r.pendientes['estatus']);
				$('#vinculados').show(1000);
			}else{

				$('#vinculados').hide();
			}
		}
	});
}


//auto vinculas
function autovincular(){
	var l = Ladda.create($("#auto-vincular-r").get(0));
	 	l.start();
	$.ajax({
		url : $url + 'autovincular',
		success : function(r){
			aviso(r);	
		},
		complete : function(x,e,o){
			ajaxComplete(x,e,o);
			l.stop();
			tabla.fnDraw();
			iniciar();			
		}
	});
}

//buscar registros  
function BuscarRegistro(id,op){
	
	$.ajax({
		url : $url + 'buscarregistro',
		data:{
			id:id,
			opera:op
			},
		success : function(r){
			//console.log(r);
			$('#opera').val(op);	
			$('#form2').show(1000);
			aplicacion2.rellenar(r);
		},
		complete : function(x,e,o){
			$('#modalTablaBusqueda').modal('hide');
			$('#lispendiente').modal('hide');
			$('#lisvinculados').modal('hide');
			ajaxComplete(x,e,o);			
		}
	});
}

function Buscarvincuado(id){

	$.ajax({
		url : $url + 'buscarvinculados',
		data:{
			id:id,
			},
		success : function(r){
			//console.log(r);
			$('#cedula').text(r.ci);
			$('#nombres').text(r.cliente);
			$('#sucursal').text(r.sucursal);
			$('#opera2').val('vinculado');
			$('#form3').show(1000);
			$id=$('#id_2').val(id);
			aplicacion3.rellenar(r);
		},
		complete : function(x,e,o){
			$('#modalTablaBusqueda').modal('hide');
			$('#lispendiente').modal('hide');
			$('#lisvinculados').modal('hide');
			ajaxComplete(x,e,o);			
		}
	});

}
//elimina
function Eliminar(id,op){

	bootbox.confirm("&iquest;Esta Seguro que Desea Eliminar Este Registro?", function(result) {
		if (!result) return;

		$.ajax({
			'url' : $url + 'eliminar',
			'data' : {
				'id' : id,
				'op' : op,
				'_method': 'delete'
			},
			'type' : 'POST',
			'success' : function(r){
				aviso(r);
				$('#form3').hide();
				$('#form2').hide();
			},
			'complete' : function(x,e,o){
				ajaxComplete(x,e,o);
				iniciar();
			}
		});
	});
}
function redirect(r){

	if(r==0){
		window.location.href = mover;
	}
}