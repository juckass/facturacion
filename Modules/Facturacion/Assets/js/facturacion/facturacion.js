var aplicacion, $form, tabla;
$(function() {
    veficar(verificar);
    $("#subir").on('click', function(e) {
        e.preventDefault();
        $("#upload:hidden").trigger('click');
    });
    aplicacion = new app("formulario", {
        'antes': function(accion) {

        },

        'limpiar': function() {
            $('#fecha_update_client').val(ultimaFecha);
        }

    });
    $('#fecha_update_client').val(ultimaFecha);

    $("#guardar2").on('click', function() {
        var l = Ladda.create($("#subir").get(0));
        l.start();

        var options2 = {
            url: $url + 'crear',
            type: 'POST',
            data: $("#formulario").serialize(),
            success: function(r) {
                aviso(r);
                resul_carga(r.id);

            },
            complete: function(x, e, o) {
                ajaxComplete(x, e, o);
                l.stop();
            }
        };

        $('#formulario').ajaxSubmit(options2);
    });

});

function veficar(veficar) {

    if (veficar == 1) {
        $('#guardar2').prop('disabled', true);
        $('#alert').show(1000);

    } else {

        $('#guardar2').prop('disabled', false);
    }
}

function resul_carga(id) {
    $.ajax({
        url: $url + 'resulcarga',
        data: 'id=' + id,
        success: function(r) {

            console.log();
            $('#total').text(r['total']);
            $('#registros').text(r['movimientos']);
            $('#guardar2').prop('disabled', true);
            $('#resumen').show(1000);

        }
    });
}