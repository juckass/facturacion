var aplicacion, tabla;
$(function() {

	tabla = $('#tabla')
	.on("click", "tbody tr", function(){
		aplicacion.buscar(this.id);
	})
	.dataTable({
		processing: true,
		serverSide: true,
		ajax: $url + "datatable",
		oLanguage: datatableEspanol,
		columns: [
			{ data: 'cedula', 	   name: 'clientes.ci'},
			{ data: 'cliente', 	   name: 'clientes.nombre'},
			{ data: 'sucursal',    name: 'sucursal.nombre'}
		]
	});

	aplicacion = new app("formulario", {
		'limpiar' : function(){
			tabla.fnDraw();
		}
	});
});