var aplicacion, tabla;
$(function() {

	tabla = $('#tabla')
	.on("click", "tbody tr", function(){
		aplicacion.buscar(this.id);
	})
	.dataTable({
		processing: true,
		serverSide: true,
		ajax: $url + "datatable",
		oLanguage: datatableEspanol,
		columns: [
			{ 
				data: 'nombre', name: 'nombre',
			},
		]
	});

	aplicacion = new app("formulario", {
		'limpiar' : function(){
			tabla.fnDraw();
		}
	});
});