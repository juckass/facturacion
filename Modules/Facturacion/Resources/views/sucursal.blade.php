@extends('admin::layouts.default')
@section('content')
	@include('admin::partials.botonera')
	<!--  tabla buscardor -->
	{{ Form::bsModalBusqueda([
		'nombre' => '100'
	], [
		'titulo' => 'Sucursales.'
	]) }}

	<!--  tabla buscardor -->
	<ul class="page-breadcrumb breadcrumb">
		<li>
			<a href="{{ url(\Config::get('admin.prefix')) }}">Inicio</a><i class="fa fa-circle"></i>
		</li>
		<li>
			<span>Sucursales</span>
		</li>
	</ul>
	<div class="row">
		{!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST']) !!}

			{{ Form::bsText('nombre', '', [
				'label' => 'Nombre de la sucursal',
				'placeholder' => 'Nombre de la sucursal',
				'required' => 'required'
			]) }}

			{{ Form::bsText('abreviatura', '', [
				'label' => 'Abreviatura',
				'placeholder' => 'Abreviatura',
				'required' => 'required'
			]) }}
			 
			{{ Form::bsNumber('cod_sucursal', '', [
				'label' => 'Codigo de sucursal',
				'placeholder' => 'Codigo de sucursal',
				'required' => 'required'
			]) }}

			{{ Form::bsNumber('correlativo', '', [
				'label' => 'Correlativo de Facturacion',
				'placeholder' => 'Corelativo de Facturacion',
				'required' => 'required'
			]) }}

		{!! Form::close() !!}
	</div>
@endsection