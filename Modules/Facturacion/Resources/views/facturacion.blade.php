@extends('admin::layouts.default')
@section('content')

	<div id="botonera">
		<div class="btn-group btn-group-solid">
			<button id="limpiar" class="btn default tooltips" data-original-title="Limpiar Pantalla" data-placement="top" data-container="body">Limpiar</button>
			<button id="guardar2" class="btn blue tooltips" data-original-title="Guardar Registro" data-placement="top" data-container="body">Guardar</button>
		</div>
	</div>
	<ul class="page-breadcrumb breadcrumb">
		<li>
			<a href="{{ url(\Config::get('admin.prefix')) }}">Inicio</a><i class="fa fa-circle"></i>
		</li>
		<li>
			<span>Facturacion</span>
		</li>
	</ul>
	<div class="alert alert-warning" id="alert">Aviso: No se puede crear  un nuevo proceso de facturacion, ya que existe uno en curso</div>
	<div class="row">
		{!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST']) !!}
			{{ Form::bsSelect('mes', 
				$controller->meses, '', [
					'label' => 'Mes',
					'required' => 'required'
			]) }}

		
			<div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
   				<label class="" for="mes">Año <i class="fa fa-asterisk requerido"></i></label>
				<select name="ano" class_cont="col-lg-3 col-md-4 col-sm-6 col-xs-12" id="ano" required="required" class="form-control">
					<option value="" selected="selected">- Seleccione</option>

					@for ($i = 2016; $i <= date('Y') ; $i++)
						
						<option value="{{ $i }}" selected="selected">{{ $i }}</option>

					@endfor

				</select>
			</div>

			<div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
				<label for="Base de datos de clientes Actualizada">Base De Datos De Clientes Actualizada</label>
				<input id="fecha_update_client" class="form-control" type="text" value="{{ $ultimaFecha }}" name="fecha_update_client" placeholder="Base De Datos De Clientes Actualizada" class_cont="col-lg-3 col-md-4 col-sm-6 col-xs-12"  readonly="readonly">
			</div>

			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<h4>Cargar Archivo <small>Excel (.xls, .xlsx, .ods)</small></h4>
		<hr>
			<input id="upload" name="subir" type="file"/>

			<button id="subir" type="button" class="btn btn-primary mt-ladda-btn ladda-button" data-style="expand-right">
				<span class="ladda-label">
					<i class="icon-arrow-right"></i> Carga archivo de movimientos
				</span>
			</button>


		{!! Form::close() !!}
	</div>
	<br>
	
	<div class="col-md-offset-4 col-lg-4" id="resumen">
		<div class="panel panel-success">
		  <div class="panel-heading">
		    <h3 class="panel-title">Resumen</h3>
		  </div>
		  <div class="panel-body">
		  	<h4>Registros: <span id="registros"></span></h4>
		 	<h4>Total de movimiento: <span id="total"></span></h4>
		  </div>
		</div>
	</div>	
@endsection
@push('js')
<script>
	var ultimaFecha= '{{$ultimaFecha}}';
	var verificar = '{{$controller->verificar()}}';
</script>
@endpush