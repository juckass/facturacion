@extends('admin::layouts.default')
@section('content')
	@include('admin::partials.botonera')
	<!--  tabla buscardor -->
	{{ Form::bsModalBusqueda([
		'cedula' => '25',
		'nombre' => '50',
		'sucursal' => '25'
	], [
		'titulo' => 'Clientes.'
	]) }}

	<!--  tabla buscardor -->
	<ul class="page-breadcrumb breadcrumb">
		<li>
			<a href="{{ url(\Config::get('admin.prefix')) }}">Inicio</a><i class="fa fa-circle"></i>
		</li>
		<li>
			<span>Clientes</span>
		</li>
	</ul>
	<div class="row">
		{!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST']) !!}

			{{ Form::bsSelect('nacionalidad', 
				[
					'V'=>'v',
					'E'=> 'E',
					'J'=> 'j',
				], '', [
					'label' => 'Nacionalidad',
					'required' => 'required'
			]) }}

			{{ Form::bsNumber('ci', '', [
				'label' => 'Numero De Cedula',
				'placeholder' => 'Numero De Cedula',
				'required' => 'required'
			]) }}

			{{ Form::bsText('nombre', '', [
				'label' => 'Nombre y Apellido',
				'placeholder' => 'Nombre y Apellido',
				'required' => 'required'
			]) }}


			{{ Form::bsText('ciudad', '', [
				'label' => 'Ciudad',
				'placeholder' => 'Ciudad',
				'required' => 'required'
			]) }}

			{{ Form::bsText('estado', '', [
				'label' => 'Estado',
				'placeholder' => 'Estado',
				'required' => 'required'
			]) }}
			 
			 {{ Form::bsSelect('sucursal_id',$controller->sucursales(), '', [
				'label' => 'sucursal',
				'required' => 'required'
			]) }}
			<br>
			<div class="form-group">
				<label class="control-label">Dirección</label>
				<textarea class="form-control" placeholder="Dirección"  rows="3" value="" id="direccion" name="direccion" style="width: 600px; height: 102px;"></textarea>
			</div>

		{!! Form::close() !!}
	</div>
@endsection
