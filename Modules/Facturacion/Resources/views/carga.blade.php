@extends('admin::layouts.default')
@section('content')
	<!--  tabla buscardor -->
	<ul class="page-breadcrumb breadcrumb">
		<li>
			<a href="{{ url(\Config::get('admin.prefix')) }}">Inicio</a><i class="fa fa-circle"></i>
		</li>
		<li>
			<span>Carga de Clientes</span>
		</li>
	</ul>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<h4>Cargar Archivo <small>Excel (.xls, .xlsx, .ods)</small></h4>
			<hr>
			{!! Form::open(['id' => 'carga', 'name' => 'carga', 'method' => 'POST']) !!}
				<input id="upload" name="subir" type="file"/>

				<button id="subir" type="button" class="btn btn-primary mt-ladda-btn ladda-button" data-style="expand-right">
					<span class="ladda-label">
						<i class="icon-arrow-right"></i> Carga archivo Excel
					</span>
				</button>
			{!! Form::close() !!}
	</div>
@endsection