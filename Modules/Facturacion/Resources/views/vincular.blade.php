@extends('admin::layouts.default')
@section('content')

	{{ Form::bsModalBusqueda([
		'Cedula' => '25',
		'Monto' => '25', 
		'Banco' => '25',
		'Fecha' => '25'
	], [
		'titulo' => 'Movimientos Rechazados.'
	]) }}

	{{ Form::bsModalBusqueda([
		'ci' => '25',
		'Monto' => '25',
		'Banco' => '25',
		'Fecha' => '25'
	], [ 
		'titulo' => 'Pendientes.',
		'id' => 'lispendiente',
		'idTabla' => 'tablaPendientes'
	]) }}

	{{ Form::bsModalBusqueda([
		'ci' => '25',
		'Monto' => '25',
		'Banco' => '25',
		'Fecha' => '25'
	], [ 
		'titulo' => 'Movimientos.',
		'id' => 'lisvinculados',
		'idTabla' => 'tablavinculados'
	]) }}

	<ul class="page-breadcrumb breadcrumb">
		<li>
			<a href="{{ url(\Config::get('admin.prefix')) }}">Inicio</a><i class="fa fa-circle"></i>
		</li>
		<li>
			<span>Vinculacion</span>
		</li>
	</ul>
	<div class="container-fluid">
		<div class="row">
			
			<div class="col-lg-4" id="pendientes">
				<div class="panel panel-success">
					<div class="panel-heading">
						<h3 class="panel-title">Movimientos Pendientes</h3>
					</div>
					<div class="panel-body">
						<h4>Pendientes: <span id="pendiente_estatus"></span></h4>
						<h4>Total en bolivares: <span id="pendiente_total"></span></h4>
						<hr>
						<center> 	
							<button type="button"  id="pendientemanual" class="btn btn-success ">Vinculacion Manual</button>
							<button type="button"  id="ver-p" class="btn btn-success ">Autovincular</button>
							<hr>	
							<a href="{{ url('/facturacion/reporte/excelpendientes') }}"><button type="button" id="imprimir" class="btn btn-success"  >Exportar Excel</button></a> 
						</center>
					</div>
				</div>
			</div>

			<div class=" col-lg-4" id="rechazados">
				<div class="panel panel-success">
					<div class="panel-heading">
						<h3 class="panel-title">Movimientos Rechazados</h3>
					</div>
					<div class="panel-body">
					<h4>Rechazados: <span id="rechazados_estatus"></span></h4>
					<h4>Total en bolivares: <span id="rechazados_total"></span></h4>
					<hr>
					<center>
						<button type="button"  id="rechazadosmanual"  data-placement="top" data-container="body" class="btn btn-success ">Vinculacion Manual</button>
						<button id="auto-vincular-r" type="button" class="btn btn-success mt-ladda-btn ladda-button" data-style="expand-right">
							<span class="ladda-label">
								<i class="icon-arrow-right">Autovincular</i> 
							</span>
						</button>
						<hr>
						<a href="{{ url('/facturacion/reporte/excelrechazado') }}"><button type="button" id="imprimir" class="btn btn-success"  >Exportar Excel</button></a> 	
					</center>
					</div>
				</div>
			</div>	

			<div class=" col-lg-4" id="vinculados">
				<div class="panel panel-success">
					<div class="panel-heading">
						<h3 class="panel-title">Movimientos Vinculados</h3>
					</div>
					<div class="panel-body">
						<h4>Vinculados: <span id="vinculados_estatus"></span></h4> 
						<h4>Pendientes vinculados: <span id="pendientes_total"></span></h4>
					<hr>
					<center>
						<a href="{{ url('/facturacion/reporte/imprimir') }}" target="_blank"><button type="button" id="imprimir" class="btn btn-success"  >Ver Reporte Pdf</button></a>
						<a href="{{ url('/facturacion/reporte/excelmovimientos') }}"><button type="button" id="imprimir" class="btn btn-success">Exportar Excel</button></a>
						<hr>
						<button type="button" id="movimientos" class="btn btn-success">Ver movimientos</button>
					</center>
					</div>
				</div>
			</div>
			<hr>
			<div class="col-lg-4" id="form1">
				<div class="panel panel-success">
					<div class="panel-heading">
						<h3 class="panel-title">Seleccione fecha</h3>
					</div>
					<div class="panel-body">
						{!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST', 'autocomplete' => 'Off']) !!}
							
							<div class="form-group ">
								<label class="" for="fecha"> Fecha <i class="fa fa-asterisk requerido"></i></label>
								<input id="fecha2" class="form-control" type="text" value="" name="fecha" placeholder="Fecha" class_cont="col-lg-3 col-md-4 col-sm-6 col-xs-12" required="required">
							</div>

						{!! Form::close() !!}
						<hr>
						<center>
							<button id="cargapendiente" type="button" class="btn btn-success mt-ladda-btn ladda-button" data-style="expand-right">
								<span class="ladda-label">
									<i class="icon-arrow-right">Autovincular</i> 
								</span>
							</button>
						</center>
					</div>
				</div>
			</div>

			<div class=" col-lg-12" id="form2">
				<div class="panel panel-success">
					<div class="panel-heading">
						<h3 class="panel-title">Movimiento Rechazado</h3>
					</div>
					<div class="panel-body">

						{!! Form::open(['id' => 'formulario2', 'name' => 'formulario2', 'method' => 'POST', 'autocomplete' => 'Off']) !!}
							
						{{ Form::bsNumber('ci', '', [
							'label' => 'Numero De Cedula',
							'placeholder' => 'Numero De Cedula',
							'required' => 'required'
						]) }}

						{{ Form::bsText('monto', '', [
							'label' => 'Monto',
							'placeholder' => 'Monto',
							'required' => 'required'
						]) }}

						{{ Form::bsText('banco', '', [
							'label' => 'banco',
							'placeholder' => 'banco',
							'required' => 'required'
						]) }}

						{{ Form::bsText('fecha', '', [
							'label' => 'Fecha',
							'required' => 'required'
						]) }}
						
						<input type="hidden" id="id" name="id" value="">
						<input type="hidden" id="opera" name="opera" value="">
				
						{!! Form::close() !!}
						<hr>
						<center> 
							<button id="manual" type="button" class="btn btn-success" data-style="expand-right">
								<span class="ladda-label">
									<i class="">Vincular</i> 
								</span>
							</button>
							<button type="button" id="eliminar" class="btn btn-danger">Eliminar</button>
						</center>
					</div>
				</div>
			</div>
			<div class=" col-lg-12" id="form3">
				<div class="panel panel-success">
					<div class="panel-heading">
						<h3 class="panel-title">Movimiento vinculado</h3>
					</div>
					<div class="panel-body">
						<div class=" col-lg-3"></div>
						<div class=" col-lg-6">
							<div class="panel panel-success">
								<div class="panel-heading">
									<h3 class="panel-title">Informacion del Cliente</h3>
								</div>
								<div class="panel-body">
								<table class="table table-responsive">
									<thead>
										<tr>
											<th>C.I</th>
											<th>Nombres y Apellidos</th>
											<th>Sucursal</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td><span id="cedula"></span></td>
											<td><span id="nombres"></span></td>
											<td><span id="sucursal"></span></td>
										</tr>
									</tbody>
								</table>
								</div>
							</div>
						</div>
						<div class=" col-lg-3"></div>
						<hr>
						<div class=" col-lg-12">
							<div class="panel panel-success">
								<div class="panel-heading">
								</div>
								{!! Form::open(['id' => 'formulario3', 'name' => 'formulario3', 'method' => 'POST', 'autocomplete' => 'Off']) !!}
										
									{{ Form::bsNumber('ci', '', [
										'label' => 'Numero De Cedula',
										'placeholder' => 'Numero De Cedula',
										'required' => 'required'
									]) }}

									{{ Form::bsText('monto', '', [
										'label' => 'Monto',
										'placeholder' => 'Monto',
										'required' => 'required'
									]) }}

									{{ Form::bsText('banco', '', [
										'label' => 'banco',
										'placeholder' => 'banco',
										'required' => 'required'
									]) }}

									{{ Form::bsText('fecha', '', [
										'label' => 'Fecha',
										'required' => 'required'
									]) }}
									
									<input type="hidden" id="id_2" name="id" value="">
									<input type="hidden" id="opera2" name="opera" value="">
							
									{!! Form::close() !!}
									<hr>
									<center> 
										<button id="movi" type="button" class="btn btn-success" data-style="expand-right">
											<span class="ladda-label">
												<i class="">Vincular</i> 
											</span>
										</button>
										<button type="button" id="eliminarmovi" class="btn btn-danger">Eliminar</button>
									</center>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
@push('js')
<script type="text/javascript">

var mover= "{{ url('/facturacion/facturacion') }}";
redirect({{ $control }});
</script>
@endpush