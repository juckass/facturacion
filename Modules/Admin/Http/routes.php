<?php

Route::group(['middleware' => 'web', 'prefix' => Config::get('admin.prefix'), 'namespace' => 'Modules\Admin\Http\Controllers'], function () {
	Route::get('/', 'escritorioController@getIndex');

	
	Route::controllers([
		'menu'			=> 'menuController',
		'perfiles'		=> 'perfilesController',
		'usuarios'		=> 'usuariosController',

		'login'			=> 'loginController',
		'escritorio'	=> 'escritorioController',
		'cambiarclave'	=> 'cambiarclaveController',
		'perfil'   		=> 'UsuarioPerfilController',
	]);
});