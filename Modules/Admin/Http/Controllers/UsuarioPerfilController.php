<?php namespace Modules\Admin\Http\Controllers;

use Modules\Admin\Http\Controllers\Controller;
use Auth;
use DB;
//Request
use Illuminate\Http\Request;
use Modules\Admin\Http\Requests\crearinfoRequest;
use Modules\Admin\Http\Requests\guardarimgRequest;

//Modelos
use Modules\Admin\Model\app_usuario as usuario;
use Modules\Admin\Model\app_usuario_perfil as user_info;

class UsuarioPerfilController extends Controller {
	

	protected $titulo = 'Perfil';

	public $autenticar = false;
	protected $css = [
		'perfil_user'
	];

	public function getIndex()
	{
		$id = Auth::user()->id;	//id del usuario que esta en la sesion
		
		return $this->view('admin::perfilusuario');
	}
	public function getBuscar(Request $request){

			 $rs =user_info::where('app_usuario_id', $request->id)->get();
			return $rs->toArray();
	}
	public function putActualizar(crearinfoRequest $request){
	 
		user_info::find($request->id)->update(
			[
				'apellido'  => $request->apellido, 
				'sexo'      => $request->sexo ,  
				'edo_civil' => $request->estado, 
				'direccion' => $request->direccion, 
				'facebook'  => $request->fb, 
				'instagram' => $request->instagram , 
				'twitter'   => $request->tw,
			]);

		usuario::find(Auth::user()->id)->update(
			[
				'nombre' 	=>$request->nombre, 
				'cedula' 	=>$request->cedula, 
				'correo' 	=>$request->correo, 
				'telefono'	=>$request->telefono, 
			]);

		return ['s' => 's', 'msj' => trans('controller.incluir')];

	}

	public function postCambio(guardarimgRequest $request){

            $file = $request->file('foto');
            $name =  Auth::user()->usuario.'.'.$file->getClientOriginalExtension();
            $path= public_path() . '/img/usuarios/';
            $file->move($path, $name);
 			$filename=$path.$name;
 			chmod($filename, 0777);

		usuario::find(Auth::user()->id)->update(
			[
				'foto'	=> $name, 
			]);

 			return ['s' => 's', 'msj' => trans('controller.incluir')];
	}	 
} 