<?php namespace Modules\Admin\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;

abstract class Controller extends BaseController {
	protected $titulo = 'Backend';
	public $prefijo_ruta = '';

	protected $patch_js = [
		'public/js',
		'Modules/Admin/Assets/js',
	];

	protected $patch_css = [
		'public/css',
		'Modules/Admin/Assets/css',
	];
}