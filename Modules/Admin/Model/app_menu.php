<?php

namespace Modules\Admin\Model;

use DB;
use URL;
use Config;
use Session;
use Modules\Admin\Model\modelo;

class app_menu extends modelo {
	protected $table = 'app_menu';
	protected $fillable = ['nombre', 'padre', 'posicion', 'direccion', 'icono'];

	protected $historico = false;

	static protected $urlSitio = '';

	static protected $metodos_excluidos = ["getMiddleware", "getRouter", "getRedirectUrl", "getValidationFactory"];
	static protected $form_metodos = ['get', 'post', 'any', 'delete', 'put'];

	static public function routers(){
		$form_metodospattern = '/^(' . implode('|', self::$form_metodos) . ')/';

		$routeCollection = \Route::getRoutes();
		$routes = [];
		$controllers = [];

		$urlBase = \Config::get('admin.prefix') . '/';
		$urlBase = preg_replace('/\/+/', '/', $urlBase);
		$urlBase = trim($urlBase, '/');
		
		$patternUrl = '/^' . $urlBase . '\//i';

		foreach ($routeCollection as $route) {
			$ruta = $route->getPath();
			if ($urlBase != '' && !preg_match($patternUrl, $ruta)){
				continue;
			}

			$ruta = preg_replace($patternUrl, '', $ruta);
			$ruta = preg_replace('/\/?\{[_a-zA-Z0-9\?]+\}\/?/', '', $ruta);
			$ruta = preg_replace('/\/index$/i', '', $ruta);

			$accion = $route->getActionName();
			$controller = preg_replace('/\@\w+$/', '', $accion);

			if (isset($controllers[$controller])){
				continue;
			}
			
			$_metodos = get_class_methods($controller);
			$_metodos = array_diff($_metodos, self::$metodos_excluidos);
			$_metodos = array_values($_metodos);
			$metodos = [];
			
			for ($i = 0, $c = count($_metodos); $i < $c; $i++) { 
				if (preg_match($form_metodospattern, $_metodos[$i])){
					$metodos[] = $_metodos[$i];
				}
			}

			$controllers[$controller] = $metodos;
			$routes[$ruta] = $controllers[$controller];
		}

		return $routes;
	}

	static public function estructura($metodos = false) {
		$form_metodospattern = '/^(' . implode('|', self::$form_metodos) . ')(.+)/';

		if ($metodos){
			$routes = self::routers();
		}

		$menus = DB::table('app_menu')
		->select('id', 'nombre', 'padre', 'posicion', 'direccion', 'icono')
		->whereNull('deleted_at')
		->orderby('posicion')
		->get();

		$data = [[
			'id' => 0,
			'text' => 'Todo',
			'li_attr' => [
				'data-direccion' => '#'
			],
			'icon' => 'fa fa-sitemap',
			'parent' => '#',
			'state' => [
				'opened' => true
			]
		]];

		foreach ($menus as $menu) {
			$menu->direccion = trim($menu->direccion);

			$data[] = [
				'id' => intval($menu->id),
				'text' => $menu->nombre,
				'li_attr' => [
					'data-direccion' => $menu->direccion
				],
				'icon' => $menu->icono,
				'parent' => intval($menu->padre)
			];

			if (!$metodos){
				continue;
			}

			if (isset($routes[$menu->direccion])){
				foreach($routes[$menu->direccion] as $metodo){
					$direccion = $menu->direccion . '/' . snake_case(preg_replace($form_metodospattern, '$2', $metodo));
					$direccion = preg_replace('/\/index$/i', '', $direccion);

					preg_match($form_metodospattern, $metodo, $match);

					$texto = ucwords(str_replace('_', ' ', snake_case($match[2]))) . ' (' . strtoupper($match[1]) . ')';

					$data[] = [
						'id' => md5($direccion),
						'text' => $texto,
						'li_attr' => [
							'data-direccion' => $direccion
						],
						'icon' => '',
						'parent' => intval($menu->id)
					];
				}
			}
		}

		return $data;
	}

	static public function estructura_permisos() {
		$menu = DB::table('app_menu')
			->select('app_menu.id', 'app_menu.nombre', 'app_menu.direccion', 'app_menu.padre', 'app_menu.icono')
			->whereNull('app_menu.deleted_at')
			->orderby('app_menu.posicion');

		$id_usuario = Session::get('id_usuario');
		$usuario = app_usuario::find($id_usuario);

		if (is_null($usuario)) {
			return [];
		}elseif (strtolower($usuario->super) === 's') {
			// si es super usuario no debe solicitar permisologia y se devuelve todo el menu
			return $menu->get();
		}

		/*
			se verifica la permisologia del usuario por perfil y por id de usuario
		*/
		$menu
		->leftJoin('app_usuario_permisos', 'app_menu.direccion', '=', 'app_usuario_permisos.ruta')
		->leftJoin('app_perfiles_permisos', 'app_menu.direccion', '=', 'app_perfiles_permisos.ruta')
		->where(function ($query) use ($usuario){
			$query
			->where('app_usuario_permisos.app_usuario_id', $usuario->id)
			->orWhere('app_perfiles_permisos.app_perfil_id', $usuario->app_perfil_id);
        })
        ->groupBy('app_menu.id', 'app_menu.nombre', 'app_menu.direccion', 'app_menu.padre', 'app_menu.icono');

		//dd($menu->toSql());
		return $menu->get();
	}

	static public function generar_menu($menu = false, $padre = 0, $contador = 0) {
		if ($menu === false) {
			$menu = self::estructura_permisos();
			//dd($menu);
		}

		if (self::$urlSitio == ''){
			$url = Config::get('admin.prefix') . '/';
			$url = preg_replace('/\/+/', '/', $url);
			$url = trim($url, '/');
			$url = URL::to($url);

			self::$urlSitio = $url;
		}
		
		$_menu = [];
		$hijos = false;
		$html = '';

		for ($i = 0, $c_menu = count($menu); $i < $c_menu; $i++) {
			if ($menu[$i]->padre == $padre) {
				$_menu[] = $menu[$i];
			}
		}

		$c__menu = count($_menu);

		for ($i = 0; $i < $c__menu; $i++) {
			$hijos = false;

			for ($j = 0; $j < $c_menu; $j++) {
				if ($menu[$j]->padre == $_menu[$i]->id) {
					$hijos = true;
					break;
				}
			};

			if (!$hijos && substr($_menu[$i]->direccion, 0, 1) === '#'){
				continue;
			}

			if ($hijos) {
				if ($contador === 0){
					$html .= '
					<li class="menu-dropdown classic-menu-dropdown">
						<a href="javascript:;">
							<i class="' . $_menu[$i]->icono . '"></i>
							' . $_menu[$i]->nombre . '
							<span class="fa fa-chevron-down"></span>
						</a>
						<ul class="dropdown-menu pull-left">' .
							self::generar_menu($menu, $_menu[$i]->id, $contador + 1) .
						'</ul>
					</li>';
				}else{
					$html .= '
					<li class="dropdown-submenu">
						<a href="javascript:;">
							<i class="' . $_menu[$i]->icono . '"></i>
							' . $_menu[$i]->nombre . '
						</a>
						<ul class="dropdown-menu">' .
							self::generar_menu($menu, $_menu[$i]->id, $contador + 1) .
						'</ul>
					</li>';
				}
			} else {
				$html .= '
				<li>
					<a href="' . self::$urlSitio . '/' . $_menu[$i]->direccion . '" class="nav-link">
						<i class="' . $_menu[$i]->icono . '"></i>
						' . $_menu[$i]->nombre . '
					</a>
				</li>';
			}
		};

		return $html;
	}
}