	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<meta name="csrf-token" content="{{ csrf_token() }}"/>

	<title>{{ ucwords($html['titulo']) }}</title>

	<!-- BEGIN GLOBAL MANDATORY STYLES -->
	<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=all" />

	<!-- Bootstrap core CSS -->
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
	
	<!-- Font awesome -->
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.2/css/font-awesome.min.css" />

	<!-- Animate -->
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.1/animate.css" />

	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.3.1/css/simple-line-icons.css">

	<!-- pnotify -->
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.0.0/pnotify.min.css" />
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.0.0/pnotify.buttons.min.css" />
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.0.0/pnotify.nonblock.min.css" />
	
	<!-- pace -->
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/themes/black/pace-theme-flash.css" />
	
	<link rel="stylesheet" type="text/css" href="{{ url('public/css/components.min.css') }}" />
	

	<link rel="stylesheet" type="text/css" href="{{ url('public/css/layout.min.css') }}" />
	<link rel="stylesheet" type="text/css" href="{{ url('public/css/themes/default.min.css') }}" />
	<link rel="stylesheet" type="text/css" href="{{ url('public/css/custom.min.css') }}" />

	<link rel="stylesheet" type="text/css" href="{{ url('public/css/backend.css') }}" />

	<link rel="apple-touch-icon" sizes="57x57" href="{{ url('public/Modules/pagina/images/favicon/apple-touch-icon-57x57.png') }}" />
	<link rel="apple-touch-icon" sizes="60x60" href="{{ url('public/Modules/pagina/images/favicon/apple-touch-icon-60x60.png') }}" />
	<link rel="apple-touch-icon" sizes="72x72" href="{{ url('public/Modules/pagina/images/favicon/apple-touch-icon-72x72.png') }}" />
	<link rel="apple-touch-icon" sizes="76x76" href="{{ url('public/Modules/pagina/images/favicon/apple-touch-icon-76x76.png') }}" />
	<link rel="apple-touch-icon" sizes="114x114" href="{{ url('public/Modules/pagina/images/favicon/apple-touch-icon-114x114.png') }}" />
	<link rel="apple-touch-icon" sizes="120x120" href="{{ url('public/Modules/pagina/images/favicon/apple-touch-icon-120x120.png') }}" />
	<link rel="apple-touch-icon" sizes="144x144" href="{{ url('public/Modules/pagina/images/favicon/apple-touch-icon-144x144.png') }}" />
	<link rel="apple-touch-icon" sizes="152x152" href="{{ url('public/Modules/pagina/images/favicon/apple-touch-icon-152x152.png') }}" />
	<link rel="apple-touch-icon" sizes="180x180" href="{{ url('public/Modules/pagina/images/favicon/apple-touch-icon-180x180.png') }}" />
	<link rel="icon" type="image/png" href="{{ url('public/Modules/pagina/images/favicon/favicon-32x32.png') }}" sizes="32x32" />
	<link rel="icon" type="image/png" href="{{ url('public/Modules/pagina/images/favicon/android-chrome-192x192.png') }}" sizes="192x192" />
	<link rel="icon" type="image/png" href="{{ url('public/Modules/pagina/images/favicon/favicon-96x96.png') }}" sizes="96x96" />
	<link rel="icon" type="image/png" href="{{ url('public/Modules/pagina/images/favicon/favicon-16x16.png') }}" sizes="16x16" />
	<link rel="manifest" href="{{ url('public/Modules/pagina/images/favicon/manifest.json') }}" />
		<link rel="stylesheet" type="text/css" href="{{ url('Modules/Admin/Assets/css/perfil_user.css') }}" />

		<link rel="stylesheet" type="text/css" href="{{ url('Modules/Admin/Assets/css/components-rounded.min.css') }}" />
	<link rel="mask-icon" href="{{ url('public/Modules/pagina/images/favicon/safari-pinned-tab.svg') }}" color="#5bbad5" />
	<meta name="msapplication-TileColor" content="#da532c" />
	<meta name="msapplication-TileImage" content="{{ url('public/Modules/pagina/images/favicon/mstile-144x144.png') }}" />
	<meta name="theme-color" content="#ffffff" />
@if (isset($html['css']))
@foreach ($html['css'] as $css)
	<link rel="stylesheet" type="text/css" href="{{ url($css) }}" />
@endforeach
@endif
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	@stack('css') 