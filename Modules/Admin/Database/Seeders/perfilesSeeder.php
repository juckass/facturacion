<?php namespace Modules\Admin\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Admin\Model\app_perfil as perfil;

class perfilesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        perfil::create([
			'nombre' => 'Administrador'
		]);

        perfil::create([
			'nombre' => 'Secretaria'
		]);
    }
}
