<?php 
return [
	'welcome'	=> 'Bienvenido',
	'submit' 	=> 'Aceptar',
	'btn_group' => [
		'clean' 	=> [
			'title' => 'Limpiar Pantalla',
			'btn' => 'Limpiar'
		],
		'save' 		=> [
			'title' => 'Guardar Registro',
			'btn' => 'Guardar'
		],
		'remove' 	=> [
			'title' => 'Eliminar Registro',
			'btn' => 'Eliminar'
		],
		'search' 	=> [
			'title' => 'Buscar',
			'btn' => 'Buscar'
		],
		'print' 	=> [
			'title' => 'Imprimir',
			'btn' => 'Imprimir'
		],
		'add' 	=> [
			'title' => 'Agregar',
			'btn' => 'Agregar'
		]
	],
	'without_permission' => 'No tiene permiso para acceder a esta area',
	'without_permission_ajax' => 'No tiene permiso para este procedimiento'
];