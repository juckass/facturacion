<?php 
return [
	'buscar'	=> 'El Registro Solicitado fue Encontrado.',
	'nobuscar'	=> 'No se Encontraron Registros',
	'incluir'	=> 'Registro Guardardo Satistactoriamente',
	'modificar'	=> 'Registro Modificado Satistactoriamente',
	'eliminar'	=> 'Registro Eliminado Satistactoriamente'
];