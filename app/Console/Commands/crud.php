<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Contracts\Bus\SelfHandling;

class crud extends Command implements SelfHandling
{
	protected $signature = 'crud {modulo} {nombre}';
	protected $description = 'Creacion de archivos';

	protected $columnas = [];
	protected $columnas_tipos = [];
	protected $tabla = '';
	protected $modulo = '';
	protected $nombre = '';
	protected $ruta = '';

	protected $timestamps = false;
	protected $softDeletes = false;

	/**
	 * Execute the command.
	 *
	 * @return void
	 */
	public function handle()
	{
		$this->modulo = strtolower($this->argument('modulo'));
		$this->nombre = $this->argument('nombre');

		$this->columnas();

		// crear control
		$this->controllers();
		$this->info('Se creo el archivo de control ' . $this->tabla. 'Controller.php');

		// crear request
		$this->requests();
		$this->info('Se creo el archivo de request ' . $this->tabla . 'Request.php');

		// crear modelo
		$this->model();
		$this->info('Se creo el archivo de modelo ' . $this->tabla . '.php');

		// crear vista
		$this->view();
		$this->info('Se creo el archivo de vista ' . $this->tabla . '.blade.php');

		// crear js
		$this->js();
		$this->info('Se creo el archivo de js ' . $this->tabla . '.js');

		// crear css
		$this->css();
		$this->info('Se creo el archivo de css ' . $this->tabla . '.css');
	}

	protected function columnas(){
		$this->tabla = $this->nombre;
		if (strpos($this->nombre, '/') !== false){
			$this->tabla = substr($this->nombre, strpos($this->nombre, '/') + 1);
			$this->ruta = substr($this->nombre, 0, strpos($this->nombre, '/') + 1);

			if (strlen($this->ruta) > 0){
				$this->ruta = '\\' . str_replace('/', '\\', $this->ruta);
			}
		}

		$this->columnas = \Schema::getColumnListing($this->tabla);
		
		if (empty($this->columnas)){
			$this->error('No existe la tabla: ' . $this->tabla);
			exit;
		}

		foreach ($this->columnas as $nombre_columna) {
			if ($nombre_columna === 'created_at' || $nombre_columna === 'updated_at'){
				$this->timestamps = true;
				continue;
			}

			if ($nombre_columna === 'deleted_at'){
				$this->softDeletes = true;
				continue;
			}

			//$this->columnas_tipos[$nombre_columna] = \DB::connection()->getDoctrineColumn($this->tabla, $nombre_columna)->getType()->getName();

			$this->columnas_tipos[$nombre_columna] = \DB::connection()->getDoctrineColumn($this->tabla, $nombre_columna);
		}

		$this->columnas = array_keys($this->columnas_tipos);
	}

	protected function archivo($nombre, $data){
		$nombre = str_replace('//', '/', $nombre);
		$directorio = substr($nombre, 0, strrpos($nombre, "/"));

		if (!is_dir($directorio)){
			mkdir($directorio, 0777, true);
		}

		if (is_file($nombre)){
			unlink($nombre);
		}

		$fp = fopen($nombre, 'w');
		fwrite($fp, $data);
		fclose($fp);
	}

	protected function nombre($nombre = ''){
		return ucwords(str_replace('_', ' ', $nombre === '' ? $this->tabla : $nombre));
	}

	protected function controllers(){
		$data = "<?php

namespace Modules\\" . studly_case($this->modulo) . "\\Http\\Controllers" . rtrim($this->ruta, '\\') . ";

//Controlador Padre
use Modules\\" . studly_case($this->modulo) . "\\Http\\Controllers\\Controller;

//Dependencias
use DB;
use Illuminate\\Http\\Request;
use yajra\\Datatables\\Datatables;

//Request
use Modules\\" . studly_case($this->modulo) . "\\Http\\Request\\" . trim($this->ruta, '\\') . $this->tabla . "Request;

//Modelos
use Modules\\" . studly_case($this->modulo) . "\\Model\\" . trim($this->ruta, '\\') . $this->tabla . ";

class " . studly_case($this->tabla) . "Controller extends Controller {
	protected \$titulo = '" . $this->nombre() . "';

	protected \$librerias = [
		'alphanum',
		'maskedinput',
		'datatables',
	];

	public function __construct(){
		parent::__construct();
	}

	public function getIndex() {
		return \$this->view('" . $this->modulo . "::" . str_replace('\\', '.', ltrim($this->ruta, '\\')) . $this->tabla . "');
	}

	public function getBuscar(Request \$request, \$id = 0){
		\$" . $this->tabla . " = " . $this->tabla . "::find(\$id);
		
		if (\$" . $this->tabla . "){
			return array_merge(\$" . $this->tabla . "->toArray(), [
				's' => 's', 
				'msj' => trans('controller.buscar')
			]);
		}
		
		return trans('controller.nobuscar');
	}

	public function postCrear(" . $this->tabla . "Request \$request){
		DB::beginTransaction();
		try{
			\$" . $this->tabla . " = " . $this->tabla . "::create(\$request->all());
		}catch(Exception \$e){
			DB::rollback();
			return \$e->errorInfo[2];
		}
		DB::commit();

		return ['s' => 's', 'msj' => trans('controller.incluir')];
	}

	public function putActualizar(" . $this->tabla . "Request \$request, \$id = 0){
		DB::beginTransaction();
		try{
			\$" . $this->tabla . " = " . $this->tabla . "::find(\$id)->update(\$request->all());
		}catch(Exception \$e){
			DB::rollback();
			return \$e->errorInfo[2];
		}
		DB::commit();

		return ['s' => 's', 'msj' => trans('controller.incluir')];
	}

	public function deleteEliminar(Request \$request, \$id = 0){
		try{
			\$" . $this->tabla . " = " . $this->tabla . "::destroy(\$id);
		}catch(Exception \$e){
			return \$e->errorInfo[2];
		}

		return ['s' => 's', 'msj' => trans('controller.eliminar')];
	}

	public function getDatatable(){
		\$sql = " . $this->tabla . "::select([
			'" . implode("', '", $this->columnas) . "'
		]);

	    return Datatables::of(\$sql)->setRowId('id')->make(true);
	}
}";
		
		$this->archivo('Modules/' . studly_case($this->modulo) . '/Http/Controllers/' . str_replace('\\', '/', $this->ruta) . studly_case($this->tabla) . 'Controller.php', $data);
	}

	protected function requests(){
		$columnas = $this->columnas_tipos;
		
		unset($columnas['id']);
		$reglas = [];

		foreach($columnas as $nombre => $columna){
			switch ($columna->getType()->getName()) {
				case 'integer':
					$reglas[] = "'" . $nombre . "' => ['required', 'integer']";
					break;
				case 'string':
					$reglas[] = "'" . $nombre . "' => ['required', 'min:3', 'max:" . $columna->getLength() . "']";
					break;
				
				default:
					$reglas[] = "'" . $nombre . "' => ['required']";
					break;
			}
			
		}


		$data = "<?php

namespace Modules\\" . studly_case($this->modulo) . "\\Http\\Requests" . rtrim($this->ruta, '\\') . ";

use App\Http\Requests\Request;
 
class " . $this->tabla . "Request extends Request {
	protected \$tabla = '" . $this->tabla . "';
	protected \$rules = [
		" . implode(", \n\t\t", $reglas) . "
	];

	public function rules(){
		return \$this->reglas();
	}
}";

		$this->archivo('Modules/' . studly_case($this->modulo) . '/Http/Requests/' . str_replace('\\', '/', $this->ruta) . $this->tabla . 'Request.php', $data);
	}

	protected function model(){
		$data = "<?php
namespace Modules\\" . studly_case($this->modulo) . "\\Model" . rtrim($this->ruta, '\\') . ";

" . ($this->softDeletes ? "use Modules\Admin\Model\modelo;" : 'use Illuminate\Database\Eloquent\Model;') . "

class " . $this->tabla . " extends " . ($this->softDeletes ? "modelo" : 'Model') . "
{
	protected \$table = '" . $this->tabla . "';
    protected \$fillable = ['" . implode("', '", $this->columnas) . "'];

    //protected \$hidden = [" . 
    	($this->timestamps ? "'created_at', 'updated_at'" : '') . 
    	($this->softDeletes ? ($this->timestamps ? ", " : '') . "'deleted_at'" : '') .
    "];
}";

		$this->archivo('Modules/' . studly_case($this->modulo) . '/Model/' . str_replace('\\', '/', $this->ruta) . $this->tabla . '.php', $data);
	}

	protected function view(){
		$columnas = $this->columnas_tipos;
		
		unset($columnas['id']);
		$campos = [];
		$thtable = [];

		foreach ($columnas as $nombre => $columna){
			$thtable[] = "'" . $this->nombre($nombre) . "' => '" . (100 / count($columnas)) . "'";

			switch ($columna->getType()->getName()) {
				case 'integer':
					$campos[] = "
			{{ Form::bsNumber('" . $nombre . "', '', [
				'label' => '" . $this->nombre($nombre) . "',
				'placeholder' => '" . $this->nombre($nombre) . "',
				'required' => 'required'
			]) }}";
					break;
				case 'string':
					$campos[] = "
			{{ Form::bsText('" . $nombre . "', '', [
				'label' => '" . $this->nombre($nombre) . "',
				'placeholder' => '" . $this->nombre($nombre) . "',
				'required' => 'required'
			]) }}";
					break;
				
				default:
					$campos[] = "
			{{ Form::bsText('" . $nombre . "', '', [
				'label' => '" . $this->nombre($nombre) . "',
				'placeholder' => '" . $this->nombre($nombre) . "',
				'required' => 'required'
			]) }}";
					break;
			}
		}

		$data = "@extends('admin::layouts.default')
@section('content')
	@include('admin::partials.botonera')

	{{ Form::bsModalBusqueda([
		" . implode(",\n\t\t", $thtable) . "
	]) }}

	<ul class=\"page-breadcrumb breadcrumb\">
		<li>
			<a href=\"{{ url('/') }}\">Inicio</a><i class=\"fa fa-circle\"></i>
		</li>
		<li>
			<span>" . $this->nombre() . "</span>
		</li>
	</ul>
	
	<div class=\"row\">
		{!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
		
		" . implode(' ', $campos) . "
		
		{!! Form::close() !!}
	</div>
@endsection";

		$this->archivo('Modules/' . studly_case($this->modulo) . '/Resources/views/' . str_replace('\\', '/', $this->ruta) . $this->tabla . '.blade.php', $data);
	}

	protected function css(){
		$data = "/* Style de " . $this->tabla . " */

";

		$this->archivo('Modules/' . studly_case($this->modulo) . '/Assets/css/' . str_replace('\\', '/', $this->ruta) . $this->tabla . '.css', $data);
	}
	protected function js(){
		$columnas = $this->columnas_tipos;
		
		unset($columnas['id']);
		$camposDT = [];

		foreach ($columnas as $nombre => $columna){
			$camposDT[] = [
				'data' => $nombre,
				'name' => $nombre
			];
		}

		$data = "var aplicacion, \$form, tabla;
\$(function() {
	aplicacion = new app('formulario', {
		/*'botones' : true,
		'teclado' : true,
		'medotos' : {
			'buscar' : 'buscar',
			'guardar' : 'crear',
			'actualizar' : 'actualizar',
			'eliminar' : 'eliminar',
		},*/
		'antes' : function(){},
		'limpiar' : function(){
			tabla.fnDraw();
		},
		'buscar' : function(){},
		'guardar' : function(){},
		'eliminar' : function(){}
	});

	\$form = aplicacion.form;

	tabla = \$('#tabla')
	.on('click', 'tbody tr', function(){
		aplicacion.buscar(this.id);
	})
	.dataTable({
		ajax: \$url + 'datatable',
		columns: " . json_encode($camposDT) . "
	});
});";

		$this->archivo('Modules/' . studly_case($this->modulo) . '/Assets/js/' . str_replace('\\', '/', $this->ruta) . $this->tabla . '.js', $data);
	}
}