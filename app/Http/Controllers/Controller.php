<?php

namespace App\Http\Controllers;

use Pingpong\Modules\Routing\Controller as BaseController;

abstract class Controller extends BaseController {
	protected $titulo = 'Backend';
	public $prefijo_ruta = '';

	public $autenticar = true;

	public $usuario = false;

	protected $ruta = '';

	protected $js = [];
	protected $patch_js = [
		'public/js',
		'public/plugins',
	];

	protected $css = [];
	protected $patch_css = [
		'public/css',
		'public/plugins',
	];

	protected $librerias = [];

	protected $_librerias = [
		'alphanum' => [
			'js' => ['https://cdnjs.cloudflare.com/ajax/libs/jquery.alphanum/1.0.24/jquery.alphanum.min.js']
		],
		'maskedinput' => [
			'js' => ['https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js']
		],
		'datatables' => [
			'css' => [
				//'https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/css/jquery.dataTables.min.css',
				'https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/css/dataTables.bootstrap.min.css'
			],
			'js' => [
				'https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/js/jquery.dataTables.min.js',
				'https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/js/dataTables.bootstrap.min.js'
			]
		],
		'jquery-ui' => [
			'css' => [
				'https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/themes/flick/jquery-ui.min.css',
				'https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/themes/flick/theme.min.css'
			],
			'js' => [
				'https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js'
			]
		],
		'jquery-ui-timepicker' => [
			'css' => [
				'https://cdnjs.cloudflare.com/ajax/libs/jquery-ui-timepicker-addon/1.6.1/jquery-ui-timepicker-addon.min.css'
			],
			'js' => [
				'https://cdnjs.cloudflare.com/ajax/libs/jquery-ui-timepicker-addon/1.6.1/jquery-ui-timepicker-addon.min.js',
				'https://cdnjs.cloudflare.com/ajax/libs/jquery-ui-timepicker-addon/1.6.1/i18n/jquery-ui-timepicker-es.js'
			]
		],
		'ckeditor' => [
			'js' => [
				'https://cdn.ckeditor.com/4.5.9/full/ckeditor.js'
			]
		],
		'jstree' => [
			'css' => [
				'https://cdnjs.cloudflare.com/ajax/libs/jstree/3.3.1/themes/default/style.min.css'
			],
			'js' => [
				'https://cdnjs.cloudflare.com/ajax/libs/jstree/3.3.1/jstree.min.js'
			]
		],
		'jcrop' => [
			'css' => [
				'https://cdnjs.cloudflare.com/ajax/libs/jquery-jcrop/0.9.12/css/jquery.Jcrop.min.css'
			],
			'js' => [
				'https://cdnjs.cloudflare.com/ajax/libs/jquery-jcrop/0.9.12/js/jquery.Jcrop.min.js',
				'https://cdnjs.cloudflare.com/ajax/libs/jquery-jcrop/0.9.12/js/jquery.color.min.js'
			]
		],
		'template' => [
			'js' => [
				'https://cdnjs.cloudflare.com/ajax/libs/blueimp-JavaScript-Templates/3.4.0/js/tmpl.min.js',
			]
		],
		'file-upload' => [
			'css' => [
				'https://cdnjs.cloudflare.com/ajax/libs/blueimp-file-upload/9.12.4/css/jquery.fileupload.min.css',
				'https://cdnjs.cloudflare.com/ajax/libs/blueimp-file-upload/9.12.4/css/jquery.fileupload-ui.min.css'
			],
			'js' => [
				'https://cdnjs.cloudflare.com/ajax/libs/blueimp-JavaScript-Templates/3.4.0/js/tmpl.min.js',
				'https://cdnjs.cloudflare.com/ajax/libs/blueimp-load-image/2.6.1/load-image.all.min.js',
				'https://cdnjs.cloudflare.com/ajax/libs/blueimp-file-upload/9.12.4/js/jquery.fileupload.min.js',
				'https://cdnjs.cloudflare.com/ajax/libs/blueimp-file-upload/9.12.4/js/jquery.fileupload-process.min.js',
				'https://cdnjs.cloudflare.com/ajax/libs/javascript-canvas-to-blob/3.3.0/js/canvas-to-blob.min.js',
				'https://cdnjs.cloudflare.com/ajax/libs/blueimp-file-upload/9.12.4/js/jquery.iframe-transport.min.js',
				'https://cdnjs.cloudflare.com/ajax/libs/blueimp-file-upload/9.12.4/js/jquery.fileupload-validate.min.js',
				'https://cdnjs.cloudflare.com/ajax/libs/blueimp-file-upload/9.12.4/js/jquery.fileupload-ui.min.js',
				'https://cdnjs.cloudflare.com/ajax/libs/blueimp-file-upload/9.12.4/js/jquery.fileupload-image.min.js',
				//'https://cdnjs.cloudflare.com/ajax/libs/blueimp-file-upload/9.12.4/js/jquery.fileupload-audio.min.js',
				//'https://cdnjs.cloudflare.com/ajax/libs/blueimp-file-upload/9.12.4/js/jquery.fileupload-video.min.js',

			]
		],
		'bootstrap-switch' => [
			'css' => [
				'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.2/css/bootstrap3/bootstrap-switch.min.css'
			],
			'js' => [
				'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.2/js/bootstrap-switch.min.js',
			]
		],
		'highcharts' => [
			'js' => [
				'https://cdnjs.cloudflare.com/ajax/libs/highcharts/4.2.5/highcharts.js',
				'https://cdnjs.cloudflare.com/ajax/libs/highcharts/4.2.5/highcharts-more.js'
			]
		],
		'highcharts-drilldown' => [
			'js' => [
				'https://cdnjs.cloudflare.com/ajax/libs/highcharts/4.2.5/modules/drilldown.js'
			]
		],
		'icheck' => [
			'css' => [
				'https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/skins/all.css'
			],
			'js' => [
				'https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js'
			]
		],
		'ladda' => [
			'css' => [
				'https://cdnjs.cloudflare.com/ajax/libs/ladda-bootstrap/0.9.4/ladda-themeless.min.css'
			],
			'js' => [
				'https://cdnjs.cloudflare.com/ajax/libs/ladda-bootstrap/0.9.4/spin.min.js',
				'https://cdnjs.cloudflare.com/ajax/libs/ladda-bootstrap/0.9.4/ladda.min.js'
			]
		],
		'touchspin' => [
			'css' => [
				'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-touchspin/3.1.1/jquery.bootstrap-touchspin.min.css'
			],
			'js' => [
				'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-touchspin/3.1.1/jquery.bootstrap-touchspin.min.js'
			]
		],
	];

	public function __construct() {
		$this->session();

		if ($this->autenticar) {
			$this->middleware('auth');
			$this->middleware('permisologia');
		}
	}

	protected function session() {
		$this->usuario = \Auth::user();
		if (is_null(\Session::get('id_usuario')) && (!is_null($this->usuario))) {
			\Session::put('id_usuario', $this->usuario->id);
			\Session::put('nombre_usuario', $this->usuario->nombre);
		}
	}

	protected function ruta() {
		if ($this->ruta != '' && !is_null(\Route::current())) {
			return $this->ruta;
		}

		if (!is_null(\Route::current())){
			$this->ruta = \Route::current()->getUri();
			$this->ruta = trim(preg_replace('/\{\w+\?\}/i', '', $this->ruta), '/');
		}
		
		return $this->ruta;
	}

	protected function view($vista, Array $variables = []) {
		return view($vista, $this->_app($variables));
	}

	public function permisologia($ruta = '') {
		if ($ruta === '') {
			$ruta = $this->ruta();
		}

		$ruta = substr($ruta, strlen(\Config::get('admin.prefix')) + 1);

		if (is_null($this->usuario)) {
			return false;
		}

		if (strtolower($this->usuario->super) === 's') {
			return true;
		}

		$sql = \DB::table('app_perfiles_permisos')
			->leftJoin('app_perfil', 'app_perfil.id', '=', 'app_perfiles_permisos.app_perfil_id')
			->where('app_perfil.id', $this->usuario->app_perfil_id)
			->where('app_perfiles_permisos.ruta', $ruta);

		$permisologia = $sql->count() > 0;

		if (!$permisologia) {
			$sql = \DB::table('app_usuario_permisos')
				->leftJoin('app_usuario', 'app_usuario.id', '=', 'app_usuario_permisos.app_usuario_id')
				->where('app_usuario.id', $this->usuario->id)
				->where('app_usuario_permisos.ruta', $ruta);

			$permisologia = $sql->count() > 0;
		}

		return $permisologia;
	}

	protected function _app(Array $arr = []) {
		return array_merge([
			'controller' => $this,
			'usuario' => \Auth::user(),
			'html' => [
				'titulo' => $this->titulo(),
				'js' => $this->_js(),
				'css' => $this->_css(),
			],
		], $arr);
	}

	public function titulo() {
		return $this->titulo;
	}

	protected function _archivos($ruta, $archivos, $ext) {
		$arr = [];
		
		if (strlen($this->prefijo_ruta) > 0){
			$archivos[] = substr($this->ruta(), strlen($this->prefijo_ruta) + 1);
		}else{
			$archivos[] = $this->ruta();
		}

		if (!is_array($ruta)) {
			$ruta = [$ruta];
		}

		$ext = str_replace('.', '', $ext);

		foreach ($archivos as $archivo) {
			if (filter_var($archivo, FILTER_VALIDATE_URL)) {
				$arr[] = $archivo;
				continue;
			}

			if (!preg_match('/^.*\.(' . $ext . ')$/i', $archivo)) {
				$archivo .= '.' . $ext;
			}

			for ($i = 0, $c = count($ruta); $i < $c; $i++) {
				if (is_file($ruta[$i] . '/' . $archivo)) {
					$arr[] = url($ruta[$i] . '/' . $archivo);
					break;
				}
			}


		}

		return $arr;
	}

	protected function librerias($tipo){
		$arr = [];

		foreach ($this->_librerias as $nombre => $dependencias) {
			if(in_array($nombre, $this->librerias)){
				foreach ($dependencias as $indice => $archivos) {
					if ($indice == $tipo){
						$arr = array_merge($arr, $archivos);
					}
				}
			}
		}
		
		return $arr;
	}

	protected function _js() {
		$arr = array_merge($this->js, $this->librerias('js'));
		return $this->_archivos($this->patch_js, $arr, '.js');
	}

	protected function _css() {
		$arr = array_merge($this->css, $this->librerias('css'));
		return $this->_archivos($this->patch_css, $arr, '.css');
	}

	public function limit_text($text, $limit) {
		if (str_word_count($text, 0) > $limit) {
			$words = str_word_count($text, 2);
			$pos = array_keys($words);
			$text = substr($text, 0, $pos[$limit]) . '...';
		}
		return $text;
	}

	protected function slugify($text) {
		// replace non letter or digits by -
		$text = preg_replace('~[^\pL\d]+~u', '-', $text);

		// transliterate
		$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

		// remove unwanted characters
		$text = preg_replace('~[^-\w]+~', '', $text);

		// trim
		$text = trim($text, '-');

		// remove duplicate -
		$text = preg_replace('~-+~', '-', $text);

		// lowercase
		$text = strtolower($text);

		if (empty($text)) {
			return 'n-a';
		}

		return $text;
	}
}