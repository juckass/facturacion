<style type="text/css">
#reporteHtml {
	position: relative;
	margin: 0 auto; 
	color: #001028;
	background: #FFFFFF;
	font-size: 12px; 
	font-family: Arial;
}

#reporteHtml .clearfix:after {
	content: "";
	display: table;
	clear: both;
}

#reporteHtml a {
	color: #5D6975;
	text-decoration: underline;
}

#reporteHtml .header {
	padding: 10px 0;
	margin-bottom: 30px;
}

#reporteHtml #logo {
	text-align: center;
	margin-bottom: 10px;
}

#reporteHtml #logo img {
	width: 90px;
}

#reporteHtml h1 {
	border-top: 1px solid  #5D6975;
	border-bottom: 1px solid  #5D6975;
	color: #5D6975;
	font-size: 2.4em;
	line-height: 1.4em;
	font-weight: normal;
	text-align: center;
	margin: 0 0 20px 0;
}

#reporteHtml #project {
	float: left;
}

#reporteHtml #project span {
	color: #5D6975;
	text-align: right;
	margin-right: 10px;
	display: inline-block;
}

#reporteHtml #company {
	float: right;
	text-align: right;
}

#reporteHtml #project div,
#reporteHtml #company div {
	white-space: nowrap;        
}
#cal{
	margin-left: 50px;
}
#reporteHtml table {
	width: 100%;
	border-collapse: collapse;
	border-spacing: 0;
	margin-bottom: 20px;
	font-family: Arial;
}

#reporteHtml table tr:nth-child(2n-1) td {
	background: #F5F5F5;
}

#reporteHtml table th,
#reporteHtml table td {
	text-align: center;
}

#reporteHtml table th {
	padding: 5px 20px;
	color: #5D6975;
	border-bottom: 1px solid #C1CED9;
	white-space: nowrap;        
	font-weight: normal;
}

#reporteHtml table .service,
#reporteHtml table .desc {
	text-align: left;
}

#reporteHtml table td {
	padding: 10px 20px;
	text-align: right;
}
#reporteHtml table td.service,
#reporteHtml table td.desc {
	vertical-align: top;
}

#reporteHtml table td.unit,
#reporteHtml table td.qty,
#reporteHtml table td.total {
	font-size: 1.2em;
}

#reporteHtml table td.grand {
	border-top: 1px solid #5D6975;;
}

#reporteHtml .footer {
	color: #5D6975;
	width: 100%;
	height: 30px;
	position: absolute;
	bottom: 0;
	border-top: 1px solid #C1CED9;
	padding: 8px 0;
	text-align: center;
}
#nom{
	margin-left: 522px;
}
</style>

<div id="reporteHtml">
	<div class="header clearfix">
		<div id="logo">
			<img src="<?php echo e(url('public/img/logos/demo.png')); ?>" style="width: 4cm;">
		</div>
		<h1>Movimientos Vinculados</h1>
		<div id="project">
	        <div><span>Total Movimientos: </span><span ><?php echo e($totalmovimientos); ?></span></div>
	        <div><span>Total Rechazados:  </span><span ><?php echo e($totalrechazados->total); ?></span></div>
	        <div><span>Total pendientes:  </span><span ><?php echo e($totalpendientes->pen); ?> </span></div>
	        <div ><span>Total PLC:</span><span ><?php echo e($totalsucursale["Puerto la cruz"]); ?></span></div>
	        <div><span>Total a Facturar:  </span><span ><?php echo e($totalafacturar); ?></span></div>
		</div>
		<div id="company" class="clearfix">
			<div><span>Fecha de impresi&oacute;n</span> <?php echo e(date('d/m/Y')); ?></div>
		</div>
	</header>
	<div class="main">  
	<table>
			<thead>
				<tr>
					<th class="service"></th>
					<?php foreach($sucursales as $sucursal): ?>
						 <th class="service"><?php echo e($sucursal->abreviatura); ?></th>
					<?php endforeach; ?>
					<th class="service" style="background-color: #D8D8D8 !important;">TOTAL</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($bancos as $banco): ?>
					 <tr>
						<td class="service"><?php echo e($banco->banco); ?></td>
						<?php foreach($sucursales as $sucursal): ?>
							 <td class="service"><?php echo e($candidades[$banco->banco][$sucursal->nombre]); ?></td>	
						<?php endforeach; ?>	
							<td class="service" style="background-color: #D8D8D8 !important;" ><?php echo e($totalbanco[$banco->banco]); ?></td>
					 </tr>
				<?php endforeach; ?>
				<tr>
				<td class="service" style="background-color: #D8D8D8 !important; ">Total sucursal</td>
				<?php foreach($sucursales as $sucursal): ?>
					<td class="service" style="background-color: #D8D8D8 !important"><?php echo e($totalsucursale[$sucursal->nombre]); ?></td>
				<?php endforeach; ?> 
				<td class="service" style="background-color: #D8D8D8 !important"><?php echo e($total); ?></td>
		 </tr>
			</tbody>
		</table>	
		
	</div>
	<div class="footer">
		pie de pagina empresa
	</div>
</div>