<?php
$html['titulo'] = 'Pagina no Encontrada';
$usuario = \Auth::user();
?>


<?php $__env->startSection('content'); ?>
	<div class="page-content-wrapper">
        <div class="page-content">
            <div class="container">
                <div class="page-content-inner">
                    <div class="row">
                        <div class="col-md-12 page-404">
                            <div class="number font-green"> 404 </div>
                            <div class="details">
                                <h3>No Encontramos lo Solicitado.</h3>
                                <p> No podemos encontrar la p&aacute;gina que est&aacute; buscando.
                                    <br>
                                    <a href="<?php echo e(url(\Config::get('admin.prefix'))); ?>"> Regresa a Inicio </a> o prueba con la barra de men&uacute;. </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('css'); ?>
<style type="text/css" media="screen">
.page-404 .number,.page-500 .number{letter-spacing:-10px;line-height:128px;font-size:128px;font-weight:300}.page-404 .details,.page-500 .details{margin-left:40px;display:inline-block}.page-404{text-align:center}.page-404 .number{position:relative;top:35px;display:inline-block;margin-top:0;margin-bottom:10px;color:#7bbbd6;text-align:right}.page-404-full-page .page-404,.page-500-full-page .page-500{margin-top:100px}.page-404 .details{padding-top:0;text-align:left}.page-500{text-align:center}.page-500 .number{display:inline-block;color:#ec8c8c;text-align:right}.page-500 .details{text-align:left}.page-404-full-page{overflow-x:hidden;padding:20px;margin-bottom:20px;background-color:#fafafa!important}.page-404-full-page .details input{background-color:#fff}.page-500-full-page{overflow-x:hidden;padding:20px;background-color:#fafafa!important}.page-500-full-page .details input{background-color:#fff}.page-404-3{background:#000!important}.page-404-3 .page-inner img{right:0;bottom:0;z-index:-1;position:absolute}.page-404-3 .error-404{color:#fff;text-align:left;padding:70px 20px 0}.page-404-3 h1{color:#fff;font-size:130px;line-height:160px}.page-404-3 h2{color:#fff;font-size:30px;margin-bottom:30px}.page-404-3 p{color:#fff;font-size:16px}@media (max-width:480px){.page-404 .details,.page-404 .number,.page-500 .details,.page-500 .number{text-align:center;margin-left:0}.page-404-full-page .page-404{margin-top:30px}.page-404-3 .error-404{text-align:left;padding-top:10px}.page-404-3 .page-inner img{right:0;bottom:0;z-index:-1;position:fixed}}


</style>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('admin::layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>