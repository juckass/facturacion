

<?php $__env->startPush('botones'); ?>
	<button id='agregar' class='btn btn-app btn-info' title='<?php echo e(Lang::get('backend.btn_group.add.title')); ?>'>
		<i class='fa fa-plus'></i>
		<span class='visible-lg-inline visible-md-inline'><?php echo e(Lang::get('backend.btn_group.add.btn')); ?></span>
	</button>

	<button id='modificar' class='btn btn-app btn-info' title='Modificar'>
		<i class='fa fa-pencil-square-o'></i>
		<span class='visible-lg-inline visible-md-inline'>Modificar</span>
	</button>
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>
	<?php echo $__env->make('admin::partials.botonera', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

	<ul class="page-breadcrumb breadcrumb">
		<li>
			<a href="<?php echo e(url(\Config::get('admin.prefix'))); ?>">Inicio</a><i class="fa fa-circle"></i>
		</li>
		<li>
			<span>Administrador</span><i class="fa fa-circle"></i>
		</li>
		<li>
			<span>Men&uacute;</span>
		</li>
	</ul>

	<div class='row'>
		<?php echo e(Form::bsText('nombre', '', [
			'label' 		=> 'Nombre',
			'placeholder' 	=> 'Nombre',
			'required' 		=> 'required'
		])); ?>


		<?php echo e(Form::bsText('direccion', '', [
			'label' 		=> 'Direcci&oacute;n',
			'placeholder' 	=> 'Direcci&oacute;n',
			'required' 		=> 'required'
		])); ?>


		<?php echo e(Form::bsText('icono', '', [
			'label' 		=> 'Icono',
			'placeholder' 	=> 'Icono'
		])); ?>


		<div class='form-group col-sm-12'>
			<label for='input_buscar'>Menu:</label>
			<input id='input_buscar' class='form-control' type='text' placeholder='Buscar' value='' /><br />

			<div id='arbol'></div>
		</div>
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin::layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>