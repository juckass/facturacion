<?php $__env->startSection('content'); ?>

	<?php echo e(Form::bsModalBusqueda([
		'Cedula' => '25',
		'Monto' => '25',
		'Banco' => '25',
		'Fecha' => '25'
	], [
		'titulo' => 'Movimientos Rechazados.'
	])); ?>


	<?php echo e(Form::bsModalBusqueda([
		'ci' => '25',
		'Monto' => '25',
		'Banco' => '25',
		'Fecha' => '25'
	], [
		'titulo' => 'Movimientos Rechazados.',
		'id' => 'modalPersonas',
		'idTabla' => 'tablaPersonas'
	])); ?>


	<ul class="page-breadcrumb breadcrumb">
		<li>
			<a href="<?php echo e(url(\Config::get('admin.prefix'))); ?>">Inicio</a><i class="fa fa-circle"></i>
		</li>
		<li>
			<span>Vinculacion</span>
		</li>
	</ul>
	<div class="container-fluid">
	<div class="row">
		
		<div class="col-lg-4" id="pendientes">
			<div class="panel panel-success">
				<div class="panel-heading">
					<h3 class="panel-title">Movimientos Pendientes</h3>
				</div>
				<div class="panel-body">
					<h4>Pendientes: <span id="pendiente_estatus"></span></h4>
					<h4>Total en bolivares: <span id="pendiente_total"></span></h4>
					<hr>
					<center> 	
					<button type="button" class="btn btn-success ">Vinculacion Manual</button>
					<button type="button"  id="ver-p" class="btn btn-success ">Autovincular</button>
				</center>
				</div>
			</div>
		</div>

		<div class=" col-lg-4" id="rechazados">
			<div class="panel panel-success">
				<div class="panel-heading">
					<h3 class="panel-title">Movimientos Rechazados</h3>
				</div>
				<div class="panel-body">
				<h4>Rechazados: <span id="rechazados_estatus"></span></h4>
				<h4>Total en bolivares: <span id="rechazados_total"></span></h4>
				<hr>
				<center> 	
					<button type="button"  id="rechazadosmanual"  data-placement="top" data-container="body" class="btn btn-success ">Vinculacion Manual</button>
					<button id="auto-vincular-r" type="button" class="btn btn-success mt-ladda-btn ladda-button" data-style="expand-right">
						<span class="ladda-label">
							<i class="icon-arrow-right">Autovincular</i> 
						</span>
					</button>
				</center>
				</div>
			</div>
		</div>	

		<div class=" col-lg-4" id="vinculados">
			<div class="panel panel-success">
				<div class="panel-heading">
					<h3 class="panel-title">Movimientos Vinculados</h3>
				</div>
				<div class="panel-body">
					<h4>Vinculados: <span id="vinculados_estatus"></h4> 
					<h4>Pendientes vinculados: <span id="pendientes_total"></h4>
				<hr>
				<center>
					<button type="button" class="btn btn-success">Mas Detalles</button>
				</center>
				</div>
			</div>
		</div>
		<hr>
		<div class="col-lg-4" id="form1">
			<div class="panel panel-success">
				<div class="panel-heading">
					<h3 class="panel-title">Seleccione fecha</h3>
				</div>
				<div class="panel-body">
					<?php echo Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST', 'autocomplete' => 'Off']); ?>

						
						<div class="form-group ">
							<label class="" for="fecha"> Fecha <i class="fa fa-asterisk requerido"></i></label>
							<input id="fecha2" class="form-control" type="text" value="" name="fecha" placeholder="Fecha" class_cont="col-lg-3 col-md-4 col-sm-6 col-xs-12" required="required">
						</div>

					<?php echo Form::close(); ?>

					<hr>
					<center>
						<button id="cargapendiente" type="button" class="btn btn-success mt-ladda-btn ladda-button" data-style="expand-right">
							<span class="ladda-label">
								<i class="icon-arrow-right">Autovincular</i> 
							</span>
						</button>
					</center>
				</div>
			</div>
		</div>

		<div class=" col-lg-12" id="form2">
			<div class="panel panel-success">
				<div class="panel-heading">
					<h3 class="panel-title">Movimiento Rechazado</h3>
				</div>
				<div class="panel-body">

					<?php echo Form::open(['id' => 'formulario2', 'name' => 'formulario2', 'method' => 'POST', 'autocomplete' => 'Off']); ?>

						
					<?php echo e(Form::bsNumber('ci', '', [
						'label' => 'Numero De Cedula',
						'placeholder' => 'Numero De Cedula',
						'required' => 'required'
					])); ?>


					<?php echo e(Form::bsText('monto', '', [
						'label' => 'Monto',
						'placeholder' => 'Monto',
						'required' => 'required'
					])); ?>


					<?php echo e(Form::bsText('banco', '', [
						'label' => 'banco',
						'placeholder' => 'banco',
						'required' => 'required'
					])); ?>


					<?php echo e(Form::bsText('fecha', '', [
						'label' => 'Fecha',
						'required' => 'required'
					])); ?>


			
					<?php echo Form::close(); ?>

					<hr>
					<center> 
						<button id="vinrechazadomanual" type="button" class="btn btn-success" data-style="expand-right">
							<span class="ladda-label">
								<i class="">Autovincular</i> 
							</span>
						</button>
					</center>
				</div>
			</div>
		</div>
	</div>
	</div>
<?php $__env->stopSection(); ?>		
<?php echo $__env->make('admin::layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>