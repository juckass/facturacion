<?php $__env->startSection('content'); ?>
	<?php echo $__env->make('admin::partials.botonera', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	<!--  tabla buscardor -->
	<?php echo e(Form::bsModalBusqueda([
		'nombre' => '100'
	], [
		'titulo' => 'Sucursales.'
	])); ?>


	<!--  tabla buscardor -->
	<ul class="page-breadcrumb breadcrumb">
		<li>
			<a href="<?php echo e(url(\Config::get('admin.prefix'))); ?>">Inicio</a><i class="fa fa-circle"></i>
		</li>
		<li>
			<span>Sucursales</span>
		</li>
	</ul>
	<div class="row">
		<?php echo Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST']); ?>


			<?php echo e(Form::bsText('nombre', '', [
				'label' => 'Nombre de la sucursal',
				'placeholder' => 'Nombre de la sucursal',
				'required' => 'required'
			])); ?>


			<?php echo e(Form::bsText('abreviatura', '', [
				'label' => 'Abreviatura',
				'placeholder' => 'Abreviatura',
				'required' => 'required'
			])); ?>

			 
			<?php echo e(Form::bsNumber('cod_sucursal', '', [
				'label' => 'Codigo de sucursal',
				'placeholder' => 'Codigo de sucursal',
				'required' => 'required'
			])); ?>


			<?php echo e(Form::bsNumber('correlativo', '', [
				'label' => 'Correlativo de Facturacion',
				'placeholder' => 'Corelativo de Facturacion',
				'required' => 'required'
			])); ?>


		<?php echo Form::close(); ?>

	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin::layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>