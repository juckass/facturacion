<!DOCTYPE html>
<!--[if IE 8]>    <html lang="es" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>    <html lang="es" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--><html lang="es"> <!--<![endif]-->
<head>
		<meta charset="utf-8" />
		<title>Inicio de Sesión</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="width=device-width, initial-scale=1" name="viewport" />
		<meta content="" name="description" />
		<meta content="" name="author" />
		<meta name="csrf-token" content="<?php echo e(csrf_token()); ?>"/>

		<!-- BEGIN GLOBAL MANDATORY STYLES -->
		<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=all" />

		<!-- Bootstrap core CSS -->
		<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />

		<!-- Font awesome -->
		<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.2/css/font-awesome.min.css" />

		<!-- Animate -->
		<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.1/animate.css" />

		<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.0.0/pnotify.min.css" />
		<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.0.0/pnotify.buttons.min.css" />
		<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.0.0/pnotify.nonblock.min.css" />


		<link rel="stylesheet" type="text/css" href="<?php echo e(asset('public/css/components.min.css')); ?>" />
		<link rel="stylesheet" type="text/css"  href="<?php echo e(asset('public/css/plugins.min.css')); ?>" />

		<link rel="stylesheet" type="text/css" href="<?php echo e(asset('Modules/Admin/Assets/css/login.min.css')); ?>" />
		<link rel="shortcut icon" href="favicon.ico" />
	</head>
	<body class="login">
		<div class="logo">
			<a href="<?php echo e(url(\Config::get('admin.prefix'))); ?>">
				<img src="<?php echo e(asset('public/img/logos/logo_blanco.png')); ?>" alt="Logo" width="284" />
			</a>
		</div>
		<div class="content">
			<?php echo Form::open(array('id' => 'formulario', 'url' => 'login')); ?>

				<h3 class="form-title font-green">Iniciar Sesión</h3>
				<div class="form-group">
					<label class="control-label visible-ie8 visible-ie9">Usuario</label>
					<?php echo Form::text('nombre', '', ['class' => 'form-control form-control-solid placeholder-no-fix', 'autocomplete' => 'off', 'placeholder' => Lang::get('login.user'), 'required' => 'required']); ?>

				</div>
				<div class="form-group">
					<label class="control-label visible-ie8 visible-ie9">Contrase&ntilde;a</label>
					<?php echo Form::password('password', ['class' => 'form-control form-control-solid placeholder-no-fix', 'autocomplete' => 'off', 'placeholder' => Lang::get('login.password')]); ?>

				</div>
				<label class="rememberme check mt-checkbox mt-checkbox-outline">
						<?php echo Form::checkbox('recordar', '1', false); ?>

						<?php echo e(Lang::get('login.remember_me')); ?>

						<span></span>
					</label>
				<div class="form-actions" style="text-align: center;">
					<?php echo Form::button(Lang::get('login.log_in'), ['class' => 'btn green uppercase']); ?>

				</div>
				<?php /* <div class="login-options">
					<h4>&Oacute; Iniciar sesión como</h4>
					<ul class="social-icons">
						<li>
							<a class="social-icon-color facebook" data-original-title="facebook" href="javascript:;"></a>
						</li>
						<li>
							<a class="social-icon-color twitter" data-original-title="Twitter" href="javascript:;"></a>
						</li>
						<li>
							<a class="social-icon-color googleplus" data-original-title="Goole Plus" href="javascript:;"></a>
						</li>
						<li>
							<a class="social-icon-color linkedin" data-original-title="Linkedin" href="javascript:;"></a>
						</li>
					</ul>
				</div> */ ?>

				<div class="create-account">
					<p><?php echo e(date('Y')); ?> &copy; Mi Salud Medicina Prepagada C.A</p>
					<!--<p>Desarrollado: Ing. Miguelangel Gutierrez</p> -->
				</div>
			<?php echo Form::close(); ?>

		</div>
		<!--[if lt IE 9]>
		<script src="../assets/global/plugins/respond.min.js"></script>
		<script src="../assets/global/plugins/excanvas.min.js"></script> 
		<![endif]-->
		

		<!-- jQuery -->
		<?php if(preg_match('/(?i)msie [5-8]/',$_SERVER['HTTP_USER_AGENT'])): ?>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<?php else: ?>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
		<?php endif; ?>
		
		<!-- Bootstrap Js -->
		<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

		<!-- Form ajax -->
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/3.51/jquery.form.min.js"></script>

		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/pace.min.js"></script>

		<!-- PNotify -->
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.0.0/pnotify.min.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.0.0/pnotify.buttons.min.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.0.0/pnotify.nonblock.min.js"></script>

		<script type="text/javascript" src="<?php echo e(asset('public/js/init.js')); ?>"></script>
		<script type="text/javascript" src="<?php echo e(asset('public/js/funciones.js')); ?>"></script>
		<script type="text/javascript" src="<?php echo e(asset('Modules/Admin/Assets/js/login.js')); ?>"></script>

		<script type="text/javascript">
			var $urlbase = "<?php echo e(URL::current()); ?>", $url = $urlbase + "/";
		</script>
	</body>
</html>