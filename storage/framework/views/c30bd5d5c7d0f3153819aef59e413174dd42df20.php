<?php $__env->startSection('content'); ?>
	<?php echo $__env->make('admin::partials.botonera', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	<!--  tabla buscardor -->
	<?php echo e(Form::bsModalBusqueda([
		'cedula' => '25',
		'nombre' => '50',
		'sucursal' => '25'
	], [
		'titulo' => 'Clientes.'
	])); ?>


	<!--  tabla buscardor -->
	<ul class="page-breadcrumb breadcrumb">
		<li>
			<a href="<?php echo e(url(\Config::get('admin.prefix'))); ?>">Inicio</a><i class="fa fa-circle"></i>
		</li>
		<li>
			<span>Clientes</span>
		</li>
	</ul>
	<div class="row">
		<?php echo Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST']); ?>


			<?php echo e(Form::bsSelect('nacionalidad', 
				[
					'V'=>'v',
					'E'=> 'E',
					'J'=> 'j',
				], '', [
					'label' => 'Nacionalidad',
					'required' => 'required'
			])); ?>


			<?php echo e(Form::bsNumber('ci', '', [
				'label' => 'Numero De Cedula',
				'placeholder' => 'Numero De Cedula',
				'required' => 'required'
			])); ?>


			<?php echo e(Form::bsText('nombre', '', [
				'label' => 'Nombre y Apellido',
				'placeholder' => 'Nombre y Apellido',
				'required' => 'required'
			])); ?>



			<?php echo e(Form::bsText('ciudad', '', [
				'label' => 'Ciudad',
				'placeholder' => 'Ciudad',
				'required' => 'required'
			])); ?>


			<?php echo e(Form::bsText('estado', '', [
				'label' => 'Estado',
				'placeholder' => 'Estado',
				'required' => 'required'
			])); ?>

			 
			 <?php echo e(Form::bsSelect('sucursal_id',$controller->sucursales(), '', [
				'label' => 'sucursal',
				'required' => 'required'
			])); ?>

			<br>
			<div class="form-group">
				<label class="control-label">Dirección</label>
				<textarea class="form-control" placeholder="Dirección"  rows="3" value="" id="direccion" name="direccion" style="width: 600px; height: 102px;"></textarea>
			</div>

		<?php echo Form::close(); ?>

	</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin::layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>