
<?php $__env->startSection('content'); ?>
	<ul class="page-breadcrumb breadcrumb">
		<li>
			<a href="<?php echo e(url(\Config::get('admin.prefix'))); ?>">Inicio</a><i class="fa fa-circle"></i>
		</li>
		<li>
			<span>Escritorio</span>
		</li>
	</ul>

	<div class="page-content-inner">
		<div class="note note-info">

		</div>
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin::layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>