<?php $__env->startSection('content'); ?>
	<ul class="page-breadcrumb breadcrumb">
		<li>
			<a href="<?php echo e(url(\Config::get('admin.prefix'))); ?>">Inicio</a><i class="fa fa-circle"></i>
		</li>
		<li>
			<span>Cierre de Facturacion</span>
		</li>
	</ul>
	<div class="row">
		<div class="col-lg-12" id="pendientes">
			<div class="panel panel-success">
				<div class="panel-heading">
					<h3 class="panel-title"></h3>
				</div>
				<div class="panel-body">
					
					<h4>Cierre de Facturacion</h4>
					<hr>
					<center>
						<button class="btn btn-primary" id="cierre">Cierre</button>	
					</center>
				</div>
			</div>
		</div>
	</div>
<?php $__env->stopSection(); ?>
<?php $__env->startPush('js'); ?>
<script type="text/javascript">
var mover= "<?php echo e(url('/facturacion/facturacion')); ?>";
redirect(<?php echo e($control); ?>);
</script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('admin::layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>